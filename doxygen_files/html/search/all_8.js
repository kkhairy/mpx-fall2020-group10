var searchData=
[
  ['idt_5fentry_5fstruct_40',['idt_entry_struct',['../structidt__entry__struct.html',1,'']]],
  ['idt_5fstruct_41',['idt_struct',['../structidt__struct.html',1,'']]],
  ['infinity_42',['infinity',['../syscall_8h.html#acfbbcde3cdf443b678cf0368c785e60e',1,'syscall.h']]],
  ['init_5fheap_43',['init_heap',['../r5_8h.html#a632fb5550678e7d65db823fb1adc2dd9',1,'r5.c']]],
  ['insertpcb_44',['insertPCB',['../pcb_8h.html#a529d9160be1f750f576a89ead11524af',1,'pcb.h']]],
  ['io_5fscheduler_45',['io_scheduler',['../syscall_8h.html#a3c1aeeeb62d7c37a3a291c815785bd0b',1,'syscall.c']]],
  ['iocb_46',['IOCB',['../structIOCB.html',1,'IOCB'],['../syscall_8h.html#afe1062510cd3fb7ad905ddd71b890fa1',1,'iocb():&#160;syscall.h']]],
  ['iod_47',['IOD',['../structIOD.html',1,'IOD'],['../syscall_8h.html#abba5756a048f2051a379118e69e8d2f8',1,'iod():&#160;syscall.h']]],
  ['isempty_48',['IsEmpty',['../r5_8h.html#abaec816a6e4dd4cb1d4e70112b01b2e5',1,'r5.c']]],
  ['itoa_49',['itoa',['../date_8h.html#a2997f50b7af6a04642d3c171aa75aead',1,'date.c']]]
];
