var searchData=
[
  ['checkalarm_137',['checkAlarm',['../R4_8h.html#a1f576ee1d06e908fc16330a4506780a9',1,'R4.c']]],
  ['checktime_138',['checkTime',['../R4_8h.html#aedb8f6a27a7ac9c762cea5e11804ed15',1,'R4.h']]],
  ['clear_5fscreen_139',['clear_screen',['../comhand_8h.html#a4953d1edcbbfc7e420c423ded1d5621a',1,'comhand.c']]],
  ['com_5fclose_140',['com_close',['../R6_8h.html#aa39f1d25e881ffac9559b2fe816fe943',1,'R6.c']]],
  ['com_5fopen_141',['com_open',['../R6_8h.html#ace05d0df1dde5e194463abb3408040b3',1,'R6.c']]],
  ['com_5fread_142',['com_read',['../R6_8h.html#acea257a7e70fd62fdd7856d2670c808b',1,'R6.c']]],
  ['com_5fwrite_143',['com_write',['../R6_8h.html#abfd6531625269be0dce4c041480042c1',1,'R6.c']]],
  ['comhand_144',['comhand',['../comhand_8h.html#aa1761099416cb821f03d16506103ce87',1,'comhand():&#160;comhand.c'],['../mpx__supt_8h.html#aa1761099416cb821f03d16506103ce87',1,'comhand():&#160;comhand.c']]],
  ['createalarm_145',['createAlarm',['../R4_8h.html#a52a992787d9ef585bb1070cf5f697424',1,'R4.h']]],
  ['createpcb_146',['createPCB',['../pcb_8h.html#aa0b099416f1924dfce51fbc4bb8044e0',1,'createPCB():&#160;pcb.h'],['../tempCommands_8c.html#aa3b385f0e9fa8cb0dc70b3603ab4e615',1,'createPCB(char *name, int class, int priority):&#160;tempCommands.c']]]
];
