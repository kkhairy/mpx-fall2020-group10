var searchData=
[
  ['serial_2ec_77',['serial.c',['../serial_8c.html',1,'']]],
  ['serial_2eh_78',['serial.h',['../serial_8h.html',1,'']]],
  ['serial_5fio_79',['serial_io',['../R6_8h.html#a8101764095581ae0e26913104211604d',1,'R6.c']]],
  ['serial_5fread_80',['serial_read',['../R6_8h.html#a0dc56cfc8a5cde954dc0cae7508aacb8',1,'R6.c']]],
  ['serial_5fwrite_81',['serial_write',['../R6_8h.html#a9d22b835b87161903cca33b51f78064c',1,'R6.c']]],
  ['set_5fpri_82',['set_pri',['../pcb_8h.html#a679a0d3e6a8a9576e7b32af6fa410fa6',1,'pcb.c']]],
  ['setdate_83',['setDate',['../date_8h.html#aa155fdc926b45ca3c91ebec313e5acf9',1,'date.h']]],
  ['settime_84',['setTime',['../date_8h.html#afc443fe98a5312e4b6f8d5fe13baf7cb',1,'date.h']]],
  ['setuppcb_85',['setupPCB',['../pcb_8h.html#ad455d56d0574d2b3dacf80de41a1b0d3',1,'pcb.h']]],
  ['show_5fall_86',['show_all',['../pcb_8h.html#a855298e6ff9491b2d1811fa6538b9757',1,'pcb.c']]],
  ['show_5fallocated_87',['show_allocated',['../r5_8h.html#a4bfb4f25c10cb5914ab3be3a30bf077c',1,'r5.c']]],
  ['show_5fblocked_88',['show_blocked',['../pcb_8h.html#aeb6d02293d10442294625fe9ee77afde',1,'pcb.c']]],
  ['show_5ffree_89',['show_free',['../r5_8h.html#af9a31dac0f3ba5091ca8de0976e6be62',1,'r5.c']]],
  ['show_5flist_90',['show_list',['../r5_8h.html#a56e247efb8c71871cd17714811b9ad31',1,'r5.h']]],
  ['show_5fpcb_91',['show_pcb',['../pcb_8h.html#a601ab89424b8f6678d72705a65f8d3c0',1,'pcb.h']]],
  ['show_5fqueue_92',['show_Queue',['../pcb_8h.html#af7f5569cafe1b9a16cbedb2296541109',1,'pcb.h']]],
  ['show_5fready_93',['show_ready',['../pcb_8h.html#a739de9f7ffcbe8e7d703811ba6a1bc97',1,'pcb.c']]],
  ['show_5fsusred_94',['show_susRed',['../pcb_8h.html#a8e0bb46bc82fce5b2a22e8bbba2c4043',1,'pcb.c']]],
  ['stack_95',['Stack',['../structStack.html',1,'Stack'],['../pcb_8h.html#a504e59726c5a48f1d3bda9cf9031aa84',1,'stack():&#160;pcb.h']]],
  ['suspendpcb_96',['suspendPCB',['../pcb_8h.html#a4bd736662dafa0fcf2c17324322100c4',1,'pcb.h']]],
  ['sys_5fcall_97',['sys_call',['../syscall_8h.html#aad8093682fcfe15c2669288f19d1c52c',1,'syscall.c']]],
  ['syscall_2eh_98',['syscall.h',['../syscall_8h.html',1,'']]]
];
