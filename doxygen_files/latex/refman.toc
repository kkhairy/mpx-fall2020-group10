\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Class Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Class List}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Alarm Struct Reference}{5}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}%
\contentsline {section}{\numberline {3.2}Alarm\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}List Struct Reference}{5}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{6}{subsection.3.2.1}%
\contentsline {section}{\numberline {3.3}Block Struct Reference}{6}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{6}{subsection.3.3.1}%
\contentsline {section}{\numberline {3.4}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}M\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}CB Struct Reference}{6}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Detailed Description}{7}{subsection.3.4.1}%
\contentsline {section}{\numberline {3.5}Context Struct Reference}{7}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Detailed Description}{7}{subsection.3.5.1}%
\contentsline {section}{\numberline {3.6}D\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}CB Struct Reference}{7}{section.3.6}%
\contentsline {subsection}{\numberline {3.6.1}Detailed Description}{8}{subsection.3.6.1}%
\contentsline {section}{\numberline {3.7}gdt\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}descriptor\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}struct Struct Reference}{8}{section.3.7}%
\contentsline {subsection}{\numberline {3.7.1}Detailed Description}{8}{subsection.3.7.1}%
\contentsline {section}{\numberline {3.8}gdt\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}entry\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}struct Struct Reference}{8}{section.3.8}%
\contentsline {subsection}{\numberline {3.8.1}Detailed Description}{9}{subsection.3.8.1}%
\contentsline {section}{\numberline {3.9}idt\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}entry\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}struct Struct Reference}{9}{section.3.9}%
\contentsline {subsection}{\numberline {3.9.1}Detailed Description}{9}{subsection.3.9.1}%
\contentsline {section}{\numberline {3.10}idt\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}struct Struct Reference}{9}{section.3.10}%
\contentsline {subsection}{\numberline {3.10.1}Detailed Description}{9}{subsection.3.10.1}%
\contentsline {section}{\numberline {3.11}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}O\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}CB Struct Reference}{9}{section.3.11}%
\contentsline {subsection}{\numberline {3.11.1}Detailed Description}{10}{subsection.3.11.1}%
\contentsline {section}{\numberline {3.12}I\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}OD Struct Reference}{10}{section.3.12}%
\contentsline {subsection}{\numberline {3.12.1}Detailed Description}{10}{subsection.3.12.1}%
\contentsline {section}{\numberline {3.13}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}M\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}CB Struct Reference}{11}{section.3.13}%
\contentsline {subsection}{\numberline {3.13.1}Detailed Description}{11}{subsection.3.13.1}%
\contentsline {section}{\numberline {3.14}param Struct Reference}{11}{section.3.14}%
\contentsline {subsection}{\numberline {3.14.1}Detailed Description}{11}{subsection.3.14.1}%
\contentsline {section}{\numberline {3.15}P\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}CB Struct Reference}{11}{section.3.15}%
\contentsline {subsection}{\numberline {3.15.1}Detailed Description}{12}{subsection.3.15.1}%
\contentsline {section}{\numberline {3.16}Queue Struct Reference}{12}{section.3.16}%
\contentsline {subsection}{\numberline {3.16.1}Detailed Description}{12}{subsection.3.16.1}%
\contentsline {section}{\numberline {3.17}Stack Struct Reference}{12}{section.3.17}%
\contentsline {subsection}{\numberline {3.17.1}Detailed Description}{13}{subsection.3.17.1}%
\contentsline {chapter}{\numberline {4}File Documentation}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/include/core/serial.h File Reference}{15}{section.4.1}%
\contentsline {section}{\numberline {4.2}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/kernel/core/kmain.c File Reference}{15}{section.4.2}%
\contentsline {section}{\numberline {4.3}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/kernel/core/serial.c File Reference}{16}{section.4.3}%
\contentsline {section}{\numberline {4.4}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}supt.c File Reference}{16}{section.4.4}%
\contentsline {section}{\numberline {4.5}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}supt.h File Reference}{17}{section.4.5}%
\contentsline {section}{\numberline {4.6}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R1/comhand.h File Reference}{18}{section.4.6}%
\contentsline {section}{\numberline {4.7}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R1/date.h File Reference}{19}{section.4.7}%
\contentsline {section}{\numberline {4.8}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R2/pcb.h File Reference}{19}{section.4.8}%
\contentsline {section}{\numberline {4.9}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R2/temp\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Commands.c File Reference}{21}{section.4.9}%
\contentsline {section}{\numberline {4.10}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R3/procsr3.h File Reference}{21}{section.4.10}%
\contentsline {section}{\numberline {4.11}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R3/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R4.h File Reference}{22}{section.4.11}%
\contentsline {section}{\numberline {4.12}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R3/syscall.h File Reference}{22}{section.4.12}%
\contentsline {subsection}{\numberline {4.12.1}Function Documentation}{23}{subsection.4.12.1}%
\contentsline {subsubsection}{\numberline {4.12.1.1}io\_scheduler()}{23}{subsubsection.4.12.1.1}%
\contentsline {section}{\numberline {4.13}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R5/r5.h File Reference}{24}{section.4.13}%
\contentsline {section}{\numberline {4.14}mpx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}core/modules/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R6/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R6.h File Reference}{25}{section.4.14}%
\contentsline {subsection}{\numberline {4.14.1}Function Documentation}{26}{subsection.4.14.1}%
\contentsline {subsubsection}{\numberline {4.14.1.1}com\_close()}{26}{subsubsection.4.14.1.1}%
\contentsline {subsubsection}{\numberline {4.14.1.2}com\_open()}{27}{subsubsection.4.14.1.2}%
\contentsline {subsubsection}{\numberline {4.14.1.3}com\_read()}{27}{subsubsection.4.14.1.3}%
\contentsline {subsubsection}{\numberline {4.14.1.4}com\_write()}{28}{subsubsection.4.14.1.4}%
\contentsline {subsubsection}{\numberline {4.14.1.5}disable\_interrupts()}{29}{subsubsection.4.14.1.5}%
\contentsline {subsubsection}{\numberline {4.14.1.6}enable\_interrupts()}{29}{subsubsection.4.14.1.6}%
\contentsline {subsubsection}{\numberline {4.14.1.7}pic\_mask()}{30}{subsubsection.4.14.1.7}%
\contentsline {subsubsection}{\numberline {4.14.1.8}serial\_io()}{30}{subsubsection.4.14.1.8}%
\contentsline {subsubsection}{\numberline {4.14.1.9}serial\_read()}{30}{subsubsection.4.14.1.9}%
\contentsline {subsubsection}{\numberline {4.14.1.10}serial\_write()}{31}{subsubsection.4.14.1.10}%
\contentsline {chapter}{Index}{33}{section*.55}%
