/*!
 * \file syscall.h
 */

#ifndef SYSCALL_H
#define SYSCALL_H
#include <system.h>

//#include "../modules/R2/pcb.h"

///Structure that contains the register values for context switching
typedef struct Context{

	u32int gs, fs, es, ds;
	u32int edi, esi, ebp, esp, ebx, edx, ecx, eax;
	u32int eip, cs, eflags;
}context;

///Stores a record of the device data transfer and its status
typedef struct IOD{
  char name[12];
  int Op_Code; //request IDLE, READ, WRITE
  struct PCB* pcb_id ; //the ID of the PCB that requested IO, its character buffer
  char * iod_buffer;
  int * iod_count;
  struct IOD* next;
	struct IOD* prev;

}iod;

///Stores an event flag and count, with pointers to the IOD head, tail, and current
typedef struct IOCB {
  int event_flag;
  int iocb_count;

  struct IOD * head;
  struct IOD * tail;
	struct IOD * current;

}iocb;


///interrupt that calls the context registers
u32int* sys_call(context* registers);

///creates the infinite process
void infinity();

///Runs the infinite process
void run_infinite();




//////// R6

/*!
+*  io_scheduler() creates an io device for the PCB requesting IO.
+*  @param (the params you give it depends on the design of your system)
*/
void io_scheduler();// call insaid sys_call look q requst
// divce avialble or we look if idle no doing any thinge or if their req in the Q

void enQ(iocb *Queue, iod* temp);//for blocked queue FIFO
	iod* deQ(iocb *Queue);
	iod* createIOD();
iocb *active;
iocb *pending;
void insertIOD();
#endif
