#include "../modules/mpx_supt.h"
#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include<stdint.h>
#include <system.h>
#include "../modules/R2/pcb.h"
#include "../modules/R1/date.h"
#include "../modules/R2/user_commands.h"
#include "R4.h"

alist list = {.head = NULL, .tail = NULL, .count = 0};

void createAlarm(char* msg, int hour, int min, int sec){
	ala *alarm = (ala*) sys_alloc_mem(sizeof(ala));
	strcpy(alarm->msg, msg);
	alarm->hour = hour;
  alarm->min = min;
  alarm->sec = sec;


	if(list.count == 0){
		load(&checkAlarm, "Alchk", 5);
    resumePCB("Alchk");
	}
	addAla(&list, alarm);

}

void addAla(alist *list, ala* alarm){//for blocked queue FIFO
	if	(alarm == NULL){
		return;
	}
	if(list->tail == NULL && list->head == NULL){
		list->head = alarm;
		list->tail = alarm;
	}
	else {
		list->tail->next = alarm;
		alarm->prev = list->tail;
		list->tail = alarm;
	}

	list->count++;
}

void removeAla(alist *list, ala *alarm){

	if(list->count == 1){
		list->head = NULL;
		list->tail = NULL;
	}
	else if ((list->head->hour == alarm->hour) && (list->head->min == alarm->min) && (list->head->sec == alarm->sec)){
		list->head = alarm->next;
		list->head->prev = NULL;
	//	list->count--;
	}
	else if((list->tail->hour == alarm->hour) && (list->tail->min == alarm->min) && (list->tail->sec == alarm->sec)){

		list->tail->next = NULL;
		list->tail = alarm->prev;
	}//
	else{
		alarm->prev->next = alarm->next;
		alarm->next->prev = alarm->prev;
	}

	alarm =NULL;
	list->count--;
}



//suspend , resume,set priority, show pcb,show all ,
//show ready ,show block ,vreat pcb,delete,block ,unblock
//// Alarm
void checkAlarm(){
	ala *temp = list.head;
	//serial_println(temp->time);
	//serial_println(temp->msg);

  while(1){
	   if(list.count > 0){
		     if(checkTime(temp->hour, temp->min,temp->sec) == 1){
              int msgSize = 50;
			        sys_req(WRITE, DEFAULT_DEVICE, temp->msg, &msgSize);
              removeAla(&list, temp);
		     }
		     else if(temp->next != NULL){
			         temp = temp->next;
		     }
		     sys_req(IDLE, DEFAULT_DEVICE, NULL , NULL);

	    }
      else if(list.count == 0){
	     sys_req(EXIT, DEFAULT_DEVICE, NULL , NULL);
     }
  }
}

int checkTime(int aHour, int aMin, int aSec){
	//serial_println(str);
	int time = Time();

	int hour = time / 10000;
	time = time % 10000;
	int min = time / 100;
	int sec = time % 100;


	if(aHour < hour || (aHour == hour && aMin < min) || (aHour == hour && aMin == min && aSec <sec)){
		return 1;
	}
	else{
		return 0;
	}
}
