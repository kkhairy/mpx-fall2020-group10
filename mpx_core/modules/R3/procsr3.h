/*!
 * \file procsr3.h
 */

#ifndef _PROCSR3_H
#define _PROCSR3_H

///initiate proc1
void proc1();
///initiate proc2
void proc2();
///initiate proc3
void proc3();
///initiate proc4
void proc4();
///initiate proc5
void proc5();


#endif
