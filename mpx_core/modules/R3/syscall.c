#include <core/serial.h>
#include "../modules/mpx_supt.h"
#include <string.h>
#include "../modules/R2/pcb.h"
#include "syscall.h"
#include "../modules/R2/user_commands.h"
#include "../modules/R6/R6.h"


extern param params;
pcb *cop = NULL;
context* savedContext;
param* params_ptr;
//iocb *active =  {.event_flag = 1,  .iocb_count = 0, .head = NULL, .tail = NULL};
//iocb *completed = {.event_flag = 0,  .iocb_count = 0, .head = NULL, .tail = NULL};

//com_open =(&(active->Event_Flag));
//com_close &(active->Event_Flag),1200);


          /* for R6*/
// Add to your IF block that checks the op code for IDLE/EXIT
// If the op code is read or write
  // Insert PCB to blocked queue
  // Insert an iod to the IO queue.
  // Call your IO scheduler that:
  // Reassign cop’s stack top and set its state accordingly.

u32int* sys_call(context* registers){
	static iod *tempIOD = NULL;
	static pcb *temp = NULL;
	params_ptr = (param*) (cop->Stack.stack_top +sizeof(context));
	if (cop == NULL){
		savedContext = registers;
	}
//	else{
	if(active->event_flag == 1){
		active->event_flag = 0;
		tempIOD = active->head;
		active->head = active->head->next;
		active->iocb_count--;
		unblockPCB(tempIOD->pcb_id);
		tempIOD = tempIOD->next;
		sys_free_mem(tempIOD);
		if(tempIOD != NULL)
			serial_io(tempIOD,COM_PORT);
	}
	if(pending->event_flag == 1){
		pending->event_flag = 0;
		tempIOD = pending->head;
		pending->head = pending->head->next;
		pending->iocb_count--;
		temp = findPCB(tempIOD->pcb_id->Process_Name);
		if(temp != NULL)
			unblockPCB(tempIOD->pcb_id);
		tempIOD = tempIOD->next;
		sys_free_mem(tempIOD);
		if(tempIOD != NULL)
			serial_io(pending->head,DEFAULT_DEVICE);
	}
	if(params.op_code == IDLE){
			cop->State = 0;
			cop->Stack.stack_top = (unsigned char*)registers;
			if(cop != NULL){
					insertPCB(cop);
			}
	}
	else if(params.op_code == EXIT){
			freePCB(cop);
	}
	else if (params.op_code == READ || params.op_code == WRITE){

			io_scheduler();
			if(cop != NULL){
					insertPCB(cop);
			}
			cop->Stack.stack_top = (unsigned char*)registers;

	}



	pcb* readyHead = deQueue(&ready);  //  dis patcher
	temp = cop;
	if(temp != NULL){
			insertPCB(temp);
	}
	if(readyHead == NULL){
		return (u32int*)savedContext;
	}
	else{
		readyHead->State = 1;
		cop = readyHead;

	}
	return (u32int*)cop->Stack.stack_top;



}

void infinite()
{
  char msg[30];
  int count=0;

	memset( msg, '\0', sizeof(msg));
	strcpy(msg, "Infinite PROCESS EXECUTING.\n");
	count = strlen(msg);

  while(1){
	sys_req( WRITE, DEFAULT_DEVICE, msg, &count);
    sys_req(IDLE, DEFAULT_DEVICE, NULL, NULL);
  }
}

void run_infinite(){
	pcb *block = NULL;
	if(findPCB(block->Process_Name) != NULL){
		if (block->Suspended==1){
           deletePCB(block);
		 }
		 else{
			sys_req(WRITE, DEFAULT_DEVICE, "Cannot remove. Not Suspended ", &bufferSize);
		 }
	}
    else
		load(&infinite,"infinite",8);
}

	////r6
void io_scheduler() {
    // Check if there are any active or completed IO processes on the dcb.
    // If completed,
    // unblock the corresponding PCB and remove it from queue
    // call com_read() or com_write() on the next iod depending on the op code.
	//iocb* device;
	iocb *device;

		iod *temp = createIOD(); //creat IOD

		if (temp== NULL){
			return;
		}
		else {
			if ( params_ptr->device_id == DEFAULT_DEVICE){
				 device = pending;

			if (params_ptr->device_id == COM_PORT){
				 device = active;
				 }

			if(device->iocb_count == 1)	{
				serial_io(temp,params_ptr->device_id);
			}
			cop->State = 2;
			//insertPCB(cop);
				//enQ(active, temp);
				//if(params_ptr->op_code == READ){
				//serial_io();
				insertIOD(device,temp);
			}
				//else if(params_ptr->op_code == WRITE){
				//serial_io();

				//}
			//}
			//else{
				//enQ(pending, temp);
				//if(params_ptr->op_code == READ){
					//serial_io();


				//}
		//else if(params_ptr->op_code == WRITE){
				//	serial_io();

				//}


		}
}


/*
=======

	////r6
	void io_scheduler() {
    // Check if there are any active or completed IO processes on the dcb.
    // If completed,
    // unblock the corresponding PCB and remove it from queue
    // call com_read() or com_write() on the next iod depending on the op code.*/


iod* createIOD(){

	iod* temp = (iod*)sys_alloc_mem(sizeof(iod)); //creat IOD
	temp->pcb_id =cop;
	strcpy(temp->name,cop->Process_Name);
	temp->Op_Code = params_ptr->op_code;
	temp->iod_buffer = params_ptr->buffer_ptr;
	temp->iod_count = params_ptr->count_ptr;
	temp->next = NULL;
	temp->prev = NULL;
	return temp;
}

void insertIOD(iod *insert_iod , iocb * device){
	if (device->iocb_count==0){
		device->head = device->tail = insert_iod;
	}
	else {
		device->tail->next=insert_iod;
		device->tail=insert_iod->next=NULL;
}
device->iocb_count ++;
}

void enQ(iocb *Queue, iod* temp){//for blocked queue FIFO
	if(Queue->tail == NULL && Queue->head == NULL){
		Queue->head = temp;
		Queue->tail = temp;
		
	}

	else {
		Queue->tail->next = temp;
		temp->prev = Queue->tail;
		Queue->tail = temp;
	}

	Queue->iocb_count++;
}

iod* deQ(iocb *Queue){
	if(Queue->head != NULL){
		iod *temp = Queue->head;
		if(Queue->iocb_count == 0){
			return NULL;
		}
		else if(Queue->iocb_count == 1){
			Queue->head = NULL;
			Queue->iocb_count--;
			return temp;
		}
		else{
			Queue->head = temp->next;
			Queue->head->prev = NULL;
			Queue->iocb_count--;
			return temp;
		}
	}
	return NULL;
}
