/*!
 * \file pcb.h
 */
#ifndef PCB_H
#define PCB_H

#include <system.h>
#define READY 0
#define RUNNING 1
#define BLOCKED 2

#define NOT_SUSPENDED 0
#define SUSPENDED 1
#define STACK_SIZE 1024
#include "../modules/R3/syscall.h"

///Stack structure with a pointer to it's top and base
typedef struct Stack { //stack area at least 1024 bytes
    unsigned char* stack_top;
    unsigned char* stack_base;
}stack;

///PCB structure with a process name, class, priority, state and suspended value
typedef struct PCB{ // PCB control block
    char Process_Name[12];       //process name at least 8 chars  ID

    int Process_Class; //idefined process as an APP (0) or SYS(1)


    int Priority;       //int between 0 and 9 lower execute after ligher value
    int State;         // READY 0,RUNNING 1,BLOCKED 2
    int Suspended;    //0 NOT SUSPENDED, 1 SUSPENDED

    stack Stack;
    struct PCB* next;    // pointer to next PCB in the same linklist
    struct PCB* prev;   // pointer to previous PCB in list

}pcb;

///Queue structure with a pointer to it's head and tail, as well as a counter
typedef struct Queue{
    int count;
    pcb* head;
    pcb* tail;
}queue;



///Allocates memory for the PCB and initializes stack to null. Returns pointer to new PCB if successful, NULL otherwise.
void freePCB();

/// adds item to the front of the queue.
void enQueue();

/// removes item from the front of the queue
pcb *deQueue();

/// finds pcb from approriate queue. returns pcb pointer if sucessfull else returns NULL
pcb *findPCB();

///finds pcb from given queue. returns pcb pointer if sucessfull else returns NULL
pcb *findInQueue();

///Allocates memory for the PCB and initializes stack to null. Returns pointer to new PCB if successful, NULL otherwise.
pcb *allocatePCB();

///Calls allocatePCB() and declares new PCB with given parameters. Returns pcb pointer if successful, NULL otherwise or invalid parameters.
pcb *setupPCB();

///Adds PCB to correct queue based block.state. Takes pointer to pcb as parameter.
void insertPCB();

///user commands
void createPCB();

///removes PCB from appropriate queue. takes queue and pcb pointeras parameter.returns "success" or "failure" .
int removePCB();

///shows all the information contained by the pcb. Name,class,status,state, priority
void show_pcb();

///shows all the pcb present in the ready queue
void show_ready();

///shows all the pcb present in the blocked queue
void show_blocked();
///shows all the pcb present in the suspended_ready queue
void show_susRed();
///shows all the pcb present in the ready queue and blocked queue
void show_all();

///removes PCB from given queue.takes queue and pcb pointeras parameter. returns "success" or "failure" .
int removeFromQueue();

///shows all the information contained by the pcb. Name,class,status,state, priority
void show_pcb();

///removes pcb from appropritate queue and frees associated memory
void deletePCB();

///shows all the items present in a particular queue
void show_Queue();

///sets the priority of a pcb.
void set_pri(char* name , int priority);

///block the pcb
void blockPCB();

///unblock the pcb
void unblockPCB();

///suspend the current pcb
void suspendPCB();

///Resume the current pcb
void resumePCB();


int bufferSize;
queue ready;
queue suspended_ready;
queue blocked;
queue suspended_blocked;

//queue ready = {.head = NULL, .tail = NULL, .count = 0};
//queue suspended_ready ={.head = NULL, .tail = NULL, .count = 0};
//queue blocked = {.head = NULL, .tail = NULL, .count = 0};
//queue suspended_blocked = {.head = NULL, .tail = NULL, .count = 0};
//
/*Module R2 focuses on processmanagement
R2 is about creating/deleting, keeping track of processes, and moving processes between
multiple queues representing process states
*/
#endif
