#include "../modules/mpx_supt.h"
#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include<stdint.h>
#include <system.h>
#include "pcb.h"
#include "../modules/R1/date.h"
#include "user_commands.h"

queue ready = {.head = NULL, .tail = NULL, .count = 0};
queue suspended_ready ={.head = NULL, .tail = NULL, .count = 0};
queue blocked = {.head = NULL, .tail = NULL, .count = 0};
queue suspended_blocked = {.head = NULL, .tail = NULL, .count = 0};

void enQueue(queue *Queue, pcb* proc){//for blocked queue FIFO
	if(Queue->tail == NULL && Queue->head == NULL){
		Queue->head = proc;
		Queue->tail = proc;
	}
	else {
		Queue->tail->next = proc;
		proc->prev = Queue->tail;
		Queue->tail = proc;
	}

	Queue->count++;
}

pcb* deQueue(queue *Queue){
	if(Queue->head != NULL){
		pcb *temp = Queue->head;		//for blocked queue FIFO
		if(Queue->count == 0){ 	// NO item in queue
			return NULL;
		}
		else if(Queue->count == 1){	// single present in queue
			Queue->head = NULL;
			Queue->count--;
			return temp;
		}
		else{
			Queue->head = temp->next;
			Queue->head->prev = NULL;
			Queue->count--;
			return temp;
		}
	}
	return NULL;
}

pcb* findPCB(char* name){
	pcb* temp = findInQueue(ready, name);
	if(temp != NULL){
		return temp;
	}
	freePCB(temp);
	pcb* temp1 = findInQueue(blocked, name);
	if(temp1 != NULL){
		return temp1;
	}
	freePCB(temp1);
	pcb* temp2 = findInQueue(suspended_ready, name);
	if(temp2 != NULL){
		return temp2;
	}
	freePCB(temp2);
	pcb* temp3 = findInQueue(suspended_blocked, name);
	if(temp3 != NULL){
		return temp3;
	}

	return NULL;
}

pcb* findInQueue(queue queue, char* name){
	pcb* temp = queue.head;
	int i = 0;
	while(i < queue.count){

		int n = strcmp(temp->Process_Name, name);
		if(n == 0){
			return temp;
		}
		temp = temp->next;
		i++;

	}
	return NULL;
}

pcb* allocatePCB(){
	//allocate 1024 bytes for stack
	int size = 1024;
  	pcb *proc = sys_alloc_mem(sizeof(pcb));
	unsigned char *base = sys_alloc_mem(size);
	proc->Stack.stack_base = base;
	proc->Stack.stack_top = base + size - sizeof(context);


	// if (pcb ==NULL || base ==NULL)
	// return NULL;
	return proc;
}

pcb* setupPCB(char* name, int class, int priority){
	pcb *block = allocatePCB();
	strcpy(block -> Process_Name, name);
	block->Process_Class = class;
	block->Priority = priority;
	block->State = 0; //0 for READY
	block->Suspended = 0; //0 for not suspended
	//block->Stack.stack_top = block->Stack.stack_base + 1024 - sizeof(context);
	return block;

}

void freePCB(pcb * block){
	stack *Stack = &(block -> Stack);
	unsigned char* area = Stack->stack_base;

	sys_free_mem(area);
	sys_free_mem(Stack);
	sys_free_mem(block);
	return;
}

///adds PCB to correct queue based block.state. takes pointer to pcb as parameter.
void insertPCB(pcb *block){
	if(findPCB(block->Process_Name) != NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process already exist  ", &bufferSize);
		return;
	}
	pcb* temp = NULL;
	if (block -> State == 0 && block -> Suspended == 0){
			temp = ready.head;
		if(temp == NULL){
			block ->next =NULL;
			block -> prev = NULL;
			ready.head = block;
			ready.tail = block;
			ready.count++;

			return;
		}
		else if (block->Priority > temp->Priority){
			temp->prev = block;
			block->next = temp;
			ready.head = block;
			ready.count++;
		}
		else if(block->Priority <= ready.tail->Priority){
			block->prev = ready.tail;
			ready.tail->next = block;
			ready.tail = block;
			ready.count++;
		}
		else{

			while(temp -> Priority >= block ->Priority){//insert in front of first one of lesse value
			//end f queue reached
				if(temp->next ==NULL){
					//block -> next = NULL;
					block -> prev = temp;
					temp ->next = block;
					ready.tail = block;
					ready.count++;
					return;

				}
				temp = temp->next;

			}
			//temp = temp->next;
			//pcb of lower priority found
			temp -> prev -> next = block;
			block -> prev = temp -> prev;
			temp -> prev = block;
			block -> next = temp;
			ready.count++;
			return;
		}

	} //end ready queue insertion

	if(block-> State == 0 && block -> Suspended ==1 ){
		//addInQueue(suspended_ready, block);
		temp = suspended_ready.head;
		if(temp == NULL){
			block ->next =NULL;
			block -> prev = NULL;
			suspended_ready.head = block;
			suspended_ready.tail = block;
			suspended_ready.count++;

			return;
		}
		else if (block->Priority > temp->Priority){
			temp->prev = block;
			block->next = temp;
			suspended_ready.head = block;
			suspended_ready.count++;
		}
		else if(block->Priority <= suspended_ready.tail->Priority){
			block->prev = suspended_ready.tail;
			suspended_ready.tail->next = block;
			suspended_ready.tail = block;
			suspended_ready.count++;
		}
		else{

			while(temp -> Priority >= block ->Priority){//insert in front of first one of lesse value
			//end f queue reached
				if(temp->next ==NULL){
					//block -> next = NULL;
					block -> prev = temp;
					temp ->next = block;
					suspended_ready.tail = block;
					suspended_ready.count++;
					return;

				}
				temp = temp->next;

			}
			//temp = temp->next;
			//pcb of lower priority found
			temp -> prev -> next = block;
			block -> prev = temp -> prev;
			temp -> prev = block;
			block -> next = temp;
			suspended_ready.count++;
			return;
		}

	}

	if(block-> State == 2 && block -> Suspended ==0 ){
		enQueue(&blocked, block);
	}

	if(block-> State == 2 && block -> Suspended ==1 ){
		enQueue(&suspended_blocked, block);
	}

}


void deletePCB(char* name){
	pcb* block = findPCB(name);
	if(block == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process does not exist ", &bufferSize);
		return;
	}
	if(strcmp(name, "infinite") == 0 && block->Suspended == 0){
		sys_req(WRITE, DEFAULT_DEVICE, "The process is not suspended ", &bufferSize);
		return;
	}
	removePCB(block);
	freePCB(block);
}

void show_pcb(char* name){
	pcb *block = findPCB(name);
	if(block == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process does not exist ", &bufferSize);
		return;
	}
	sys_req(WRITE, DEFAULT_DEVICE,"\nName\t:  ", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, block->Process_Name, &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE,"\nClass\t:  ", &bufferSize);
	char class[12];
	itoa(block->Process_Class,class);
	sys_req(WRITE, DEFAULT_DEVICE, class, &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE,"\nStatus\t:  ", &bufferSize);
	if(block->Suspended == 1){
		sys_req(WRITE, DEFAULT_DEVICE, "Suspended", &bufferSize);
	}
	else{
		sys_req(WRITE, DEFAULT_DEVICE, "Not Suspended", &bufferSize);
	}

	sys_req(WRITE, DEFAULT_DEVICE,"\nState\t:  ", &bufferSize);
	if(block->State == 0){
		sys_req(WRITE, DEFAULT_DEVICE, "Ready", &bufferSize);
	}
	else if (block->State == 1){
		sys_req(WRITE, DEFAULT_DEVICE, "Running", &bufferSize);
	}
	else {
		sys_req(WRITE, DEFAULT_DEVICE, "Blocked", &bufferSize);
	}

	sys_req(WRITE, DEFAULT_DEVICE,"\nPriority:  ", &bufferSize);
	char priority [12];
	itoa(block->Priority,priority);
	sys_req(WRITE, DEFAULT_DEVICE, priority, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,"\n", &bufferSize);
}

void show_ready(){
	sys_req(WRITE, DEFAULT_DEVICE,"\nReady Queue:\n", &bufferSize);
	show_Queue(ready);

}

void show_blocked(){
	sys_req(WRITE, DEFAULT_DEVICE,"\nBlocked Queue:\n", &bufferSize);
	show_Queue(blocked);
}

void show_susRed(){
	sys_req(WRITE, DEFAULT_DEVICE,"\nSuspended Ready Queue:\n", &bufferSize);
	show_Queue(suspended_ready);
}

void show_susBlo(){
	sys_req(WRITE, DEFAULT_DEVICE,"\nSuspended Blocked Queue:\n", &bufferSize);
	show_Queue(suspended_blocked);
}


void show_Queue(queue Queue){
	int i = 0;
	pcb* temp = Queue.head;
	if(Queue.count == 0){
			sys_req(WRITE, DEFAULT_DEVICE,"\n The queue is empty  \n", &bufferSize);

	}
	while(i < Queue.count){

    		char* proc = temp-> Process_Name;
    		show_pcb(proc);


		temp = temp->next;
		i++;
  	}
	sys_req(WRITE, DEFAULT_DEVICE,"\n", &bufferSize);
}

void createPCB(char* name, int class, int priority){
  	//needs to perform error checking
	//check name, class, priority
	if(strlen(name) > 12){
		sys_req(WRITE, DEFAULT_DEVICE, "\nThe process name is too long. Should be less than 12 \n ", &bufferSize);
		return;
	}
	if(findPCB(name) != NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "\nThe process already exist\n ", &bufferSize);
		return;
	}
	if(class != 0 && class != 1){
		sys_req(WRITE, DEFAULT_DEVICE, "\nThe class does not exist. Should be either 0 or 1 \n ", &bufferSize);
		return;
	}
	if( priority >9 || priority <= 0 ){
		sys_req(WRITE, DEFAULT_DEVICE, "\nThe priority should be between 0 - 9\n ", &bufferSize);
		return;
	}
  	pcb* new = setupPCB(name, class, priority);
	insertPCB(new);
	sys_req(WRITE, DEFAULT_DEVICE, "\nThe priority is created\n ", &bufferSize);
}

void show_all(){
	show_ready();
	show_blocked();
	show_susRed();
	show_susBlo();
}



int removePCB(pcb* block){//removes a pcb from appropriate queues
	int status = block->Suspended;
	int state = block->State;

	if(state == 0 && status == 0){
		return removeFromQueue(&ready, block);
	}
	else if(state == 2 && status == 0){
		return removeFromQueue(&blocked, block);
	}
	else if(state == 0 && status == 1){
		return removeFromQueue(&suspended_ready, block);
	}

	else if(state == 2 && status == 1){
		return removeFromQueue(&suspended_blocked, block);
	}
	return -1;
}

//removes a pcb from given queues
int removeFromQueue(queue *Queue, pcb* block){

	char* name = block->Process_Name;
	if(findPCB(name) == NULL){
		return -1;
	}
	if(Queue->count == 1){
		Queue->head = NULL;
		Queue->tail = NULL;
	}
	else if(strcmp(Queue->head->Process_Name, name) == 0){

		Queue->head = block->next;
		Queue->head->prev = NULL;

	}
	else if(strcmp(Queue->tail->Process_Name, name) == 0){

		Queue->tail->next = NULL;
		Queue->tail = block->prev;
	}
	else{
		block->prev->next = block->next;
		block->next->prev = block->prev;

	}
		block =NULL;
	Queue->count--;
	return 1;
}

// user command to be called
// finds pcb and removes it from appropriate queue then frees memory

void blockPCB(char *name){

	pcb* block = findPCB(name);
	if(strcmp(name, "IDLE") == 0 ){
		sys_req(WRITE, DEFAULT_DEVICE, "The IDLE process is system cannot be blocked", &bufferSize);
		return;
	}
	if(block == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process does not exist ", &bufferSize);
		return;
	}
	int state = block->State;
	if(state == 2 ){

		sys_req(WRITE, DEFAULT_DEVICE, "Process Already Blocked ", &bufferSize);

		return;
	}
   	else if(state == 0 ){

		removeFromQueue(&ready, block);
		   block->State = 2;
		insertPCB(block);

	}
		sys_req(WRITE, DEFAULT_DEVICE, "Process  sucessfully blocked ", &bufferSize);
}

void unblockPCB(char *name){

	pcb* block = findPCB(name);
	if(strcmp(name, "IDLE") == 0 ){
		sys_req(WRITE, DEFAULT_DEVICE, "The IDLE process is system cannot be unblocked", &bufferSize);
		return;
	}
	if(block == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process does not exist ", &bufferSize);
		return;
	}

	int state = block->State;
	if(state == 0 ){
		sys_req(WRITE, DEFAULT_DEVICE, "The process is already unblocked", &bufferSize);
		return;
   	 }

	else if(state == 2 ){

		removeFromQueue(&blocked, block);
		block->State = 0;
		insertPCB(block);
	}

	sys_req(WRITE, DEFAULT_DEVICE, "Process sucessfully unblocked ", &bufferSize);

}

void suspendPCB(char *name){
	pcb* block = findPCB(name);
	if(strcmp(name, "IDLE") == 0 ){
		sys_req(WRITE, DEFAULT_DEVICE, "The IDLE process is system cannot be suspended", &bufferSize);
		return;
	}
	if(block == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process does not exist ", &bufferSize);
		return;
	}
	removePCB(block);
	block->Suspended = 1;
	insertPCB(block);

}

void resumePCB(char *name){
	pcb* block = findPCB(name);
	if(block == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process does not exist ", &bufferSize);
		return;
	}
	removePCB(block);
	block->Suspended = 0;
	insertPCB(block);

}

void set_pri(char* name, int priority){
	pcb* temp = findPCB(name);
	if(strcmp(name, "IDLE") == 0 ){
		sys_req(WRITE, DEFAULT_DEVICE, "The IDLE process is system cannot change priority", &bufferSize);
		return;
	}
	if(temp == NULL){
		sys_req(WRITE, DEFAULT_DEVICE, "The process name does not exist", &bufferSize);
		return;
   	 }
   	 if(!(priority >= 0 && priority <=9) ){
		sys_req(WRITE, DEFAULT_DEVICE, "The priority should be between 0-9", &bufferSize);
		return;
   	 }
   	 temp->Priority = priority;
   	 if(temp->State == 0){
   	 		removeFromQueue(&ready, temp);
   	 	   	 insertPCB(temp);
   	 }
}
/*
void createAlarm(char* msg, char* time){
	ala *alarm = (ala*) sys_alloc_mem(sizeof(ala));
	strcpy(alarm->msg, msg);
	strcpy(alarm->time, time);
	//sys_req(WRITE, DEFAULT_DEVICE, "HELLOOLAsldaksd",(int*) 50);
	//serial_println(msg);
	//sys_req(WRITE, DEFAULT_DEVICE, msg,(int*) 50);
	//serial_println(time);

	if(list.count == 0){
		load(&checkAlarm, "Alchk", 5);
	}
	addAla(&list, alarm);

}

void addAla(alist *list, ala* alarm){//for blocked queue FIFO
	if	(alarm == NULL){
		return;
	}
	if(list->tail == NULL && list->head == NULL){
		list->head = alarm;
		list->tail = alarm;
	}
	else {
		list->tail->next = alarm;
		alarm->prev = list->tail;
		list->tail = alarm;
	}

	list->count++;
}

void removeAla(alist *list, ala *alarm){

	if(list->count == 1){
		list->head = NULL;
		list->tail = NULL;
	}
	else if (strcmp(list->head->time, alarm->time) == 0){
		list->head = alarm->next;
		list->head->prev = NULL;
	//	list->count--;
	}
	else if(strcmp(list->tail->time, alarm->time) == 0){

		list->tail->next = NULL;
		list->tail = alarm->prev;
	}//
	else{
		alarm->prev->next = alarm->next;
		alarm->next->prev = alarm->prev;
	}

	alarm =NULL;
	list->count--;
}



//suspend , resume,set priority, show pcb,show all ,
//show ready ,show block ,vreat pcb,delete,block ,unblock
//// Alarm
void checkAlarm(){
	ala *temp = list.head;
	//serial_println(temp->time);
	//serial_println(temp->msg);

	while(list.count > 0){
		if(checkTime(temp->time) == 1){
			sys_req(WRITE, DEFAULT_DEVICE, temp->msg, (int*)50);
			serial_println("2222");

			removeAla(&list, temp);

			//if( list.count == 0){
				//sys_req(EXIT, DEFAULT_DEVICE, NULL , NULL);
			//}
		}
		else if(temp->next != NULL){
			temp = temp->next;
		}
		sys_req(IDLE, DEFAULT_DEVICE, NULL , NULL);

	}
	sys_req(EXIT, DEFAULT_DEVICE, NULL , NULL);
}

int checkTime(char* str){
	//serial_println(str);
	int time = Time();

	int hour = time / 10000;
	time = time % 10000;
	int min = time / 100;
	int sec = time % 100;

	char* token = strtok(str,":");
	int aHour = atoi(token);
	token = strtok(NULL,":");
	int aMin = atoi(token);
	token = strtok(NULL,"\r");
	int aSec = atoi(token);
	//serial_println(p);
	//sys_req(WRITE, DEFAULT_DEVICE, token, &bufferSize);

	if(aHour >= hour && (aHour == hour && aMin >= min) && (aHour == hour && aMin == min && aSec >=sec)){
		return 1;
	}
	else{
		return 0;
	}
}


*/
