#include "../modules/mpx_supt.h"
#include "../modules/R2/pcb.h"
#include "../modules/R3/syscall.h"
#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include <stdint.h>

void load(void (*func) (void), char* name, int priority){
	pcb* new_pcb = setupPCB(name, 0, priority);
	context* cp = (context*) (new_pcb->Stack.stack_top);
	memset( cp, 0 , sizeof(context));
	cp -> fs = 0x10;
	cp -> gs = 0x10;
	cp -> ds = 0x10;
	cp -> es = 0x10;
	cp -> cs = 0x8;
	cp -> ebp = (u32int)(new_pcb ->Stack.stack_base);
	cp -> esp = (u32int)(new_pcb ->Stack.stack_top);
	cp -> eip = (u32int)func;
	cp -> eflags = 0x202;
	new_pcb->State = 0;
	new_pcb->Suspended = 1;
	insertPCB(new_pcb);
}
