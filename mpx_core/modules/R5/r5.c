#include "../modules/mpx_supt.h"
#include "r5.h"
#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include<stdint.h>
#include <system.h>
#include "../modules/R1/date.h"
#include <mem/heap.h>
#include "../modules/R2/pcb.h"

cmcb *freeList;
cmcb *allocList;
u32int* heapAddr;
u32int memory;
u32int allocMem;

// int bufferSize = 100;
//Initialize the heap
// CMCB  is complete memorycontrol blocks
//limited memory control blocks(LMCB)

//This function will be used to allocate all the memory available for your MPX
void init_heap(){
	int totalSize = 50000 - sizeof(cmcb) - sizeof(lmcb)-sizeof(stack);
	memory = totalSize;
	heapAddr = (u32int*)kmalloc(totalSize);
	freeList = makeMemBlock(totalSize, heapAddr, 0, NULL, NULL);
	freeList->name = "init_heap";

	allocList = NULL;



}

/*void init_heap(int size){
	int totalSize = size + sizeof(cmcb) + sizeof(lmcb);
	memory = totalSize;

	heapAddr = (u32int*)kmalloc(totalSize);
	freeList = makeMemBlock(totalSize, heapAddr, 0, NULL, NULL);
	freeList->name = "init_heap";

	allocList = NULL;



}*/

cmcb* makeMemBlock(int size, u32int addr, int type, cmcb* next, cmcb* prev){
	cmcb* Cmcb = (cmcb*)  addr;
	Cmcb->type = type;
	Cmcb->beginAddress = addr + sizeof(cmcb);
	Cmcb->size = size;
	Cmcb->memorySize = size - sizeof(lmcb) - sizeof(cmcb) - sizeof(stack);
	Cmcb->next = next;
	Cmcb->prev = prev;
	lmcb* Lmcb = (lmcb*)(size - sizeof(lmcb));
	Lmcb->type = type;
	Lmcb->size = size;
	Lmcb->memorySize = size - sizeof(lmcb);
	return Cmcb;
}

/*This function will be used to allocate memory from the heap
It will take an integer parameter indicating the amount of bytes to be allocated from the heap*/

u32int allocate(u32int bytes){

	//freeList to find a block big enough to hold the parameter size + sizeof(CMCB) + sizeof(LMCB)

	cmcb* temp = freeList;
	int reqMem = bytes + sizeof(lmcb) + sizeof(cmcb) + sizeof(stack) ;

	if(freeList== NULL){
		sys_req(WRITE, DEFAULT_DEVICE,"\n The free memory is empty  \n", &bufferSize);
		return NULL;
	}


	if((int)(memory - allocMem) < reqMem){
		return NULL;
	}

	while(temp != NULL){
		if(temp->size >=reqMem){
			int freeSize = temp->size - reqMem;
			cmcb* free = makeMemBlock(freeSize, (u32int)(temp->beginAddress + reqMem- sizeof(cmcb)), 0, temp->prev, temp->next );
			temp->next->prev = free;
			temp->type = 0;
			temp->prev->next = free;
			freeList=free;

			break;
		}
		else{
			temp = temp->next;
		}
	}
	cmcb* alloc = makeMemBlock(reqMem,( u32int )temp, 1, NULL, NULL );
	 // her if it null it will become like free and we will allcate the alloc we have
	if(allocList == NULL){
		allocList = alloc;
		return allocList->beginAddress;
	}
	allocMem += reqMem;

	cmcb* curr = allocList;
	while(1){
		if(allocList == NULL){
			allocList = alloc;
		}
		else if(alloc< curr){
			alloc->next = curr;
			alloc->prev = curr->prev;
			alloc->prev->next = alloc;
			curr->prev = alloc;
			if(allocList== curr){
				allocList = alloc;
			}
			break;
		}
		else if(curr->next == NULL) {
			curr->next = alloc;
			alloc->prev = curr;
			break;
		}
		curr= curr->next;
	}
	return alloc->beginAddress;
}


int IsEmpty(){
	return (allocList == NULL);
}


void show_list(cmcb* list){
		int bufferSize = 100;

	int i = 0;
	cmcb* temp = list;
	if(list == NULL){
		sys_req(WRITE, DEFAULT_DEVICE,"\n The list is empty  \n", &bufferSize);
	}
	while(temp != NULL){
		//serial_println("Working");

		char size [15];
		char addr[15];
		itoa(temp->memorySize, size);
		itoa((int)(temp->beginAddress), addr);

		char sCmcb[3];
		char sLmcb[3];

		char sStack[3];

		itoa(sizeof(cmcb), sCmcb);
		itoa(sizeof(lmcb), sLmcb);
		itoa(sizeof(stack), sStack);

		sys_req(WRITE, DEFAULT_DEVICE,"Begin Address:\t", &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE, addr, &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,"\nSize:\t", &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,size, &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,"\nCMCB:\t", &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,sCmcb, &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,"\nLMCB:\t", &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,sLmcb, &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,"\nStack:\t", &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE,sStack, &bufferSize);

		sys_req(WRITE, DEFAULT_DEVICE,"\n", &bufferSize);

		temp = temp->next;
		i++;
	}
}

void show_free(){
	int bufferSize = 100;
	sys_req(WRITE, DEFAULT_DEVICE,"\n Free memory list:  \n", &bufferSize);
	show_list(freeList);
}

void show_allocated(){
	int bufferSize = 100;
	sys_req(WRITE, DEFAULT_DEVICE,"\n Allocated memory list:  \n", &bufferSize);
	show_list(allocList);
}

//This procedure will be used to free aparticular block of memory that was previously allocated
int Free_mem(u32int addr){
	int bufferSize = 100;

	cmcb* temp = allocList;
	if(allocList == NULL){
		sys_req(WRITE, DEFAULT_DEVICE,"\n The allocated memory is empty  \n", &bufferSize);
	}

	while(temp != NULL){
		if(temp->beginAddress == addr){//set to allocated
			if(temp  == allocList){
				allocList = allocList->next;
			}
			temp->prev->next = temp->next;
			temp->next->prev = temp ->prev;
			temp->type = 0;
			break;
		}

		temp = temp->next;

	}

	if (temp ==  NULL){
		return 0 ;
	}


	if(freeList == NULL){
		freeList = temp;
		return 0;
	}

	cmcb* curr = freeList;
	int head = 0;
	while(curr != NULL){
		if(temp< curr){

			if(head == 0){

				temp->prev = NULL;
				temp->next = curr;
				curr->prev= temp;
				freeList = temp;
			}
			else{

				temp->next = curr;
				temp->prev = curr->prev;
				temp->prev->next = temp;
				curr->prev = temp;
			}
			break;
		}
		else if(curr->next == NULL) {
			curr->next = temp;
			temp->prev = curr;
			temp->next = NULL;
			break;
		}
		curr= curr->next;
		head = 1;
	}

	//to merge
	curr = freeList;
	int cond = 1;
	while(cond){
		if(curr->beginAddress + curr->size   == curr->next->beginAddress){

			curr->size +=  curr->next->size;
			curr->memorySize += curr->next->memorySize;


			curr->next = curr->next->next;
			curr->next->prev = curr;
			cond = 1;

		}
		else{
			curr = curr->next;
		}
		if(curr == NULL){
			break;
		}
	}
	return 0;

}

//◦ Initialize heapAddr - Temporary
//◦ Allocate Memory - Temporary
//◦ Free Memory – Temporary
//◦ IsEmpty - Temporary
//◦ Show Free Memory
//◦ Show Allocated Memory
