/*!
 * \file r5.h
 */

#ifndef R5_H
#define R5_H
#include <system.h>
#include "../modules/mpx_supt.h"
 
typedef struct CMCB{
int type;// 0 = free , 1 = not free
int size;
int memorySize;
u32int beginAddress;
char *name;

struct CMCB *next;
struct CMCB *prev;

}cmcb;

typedef struct LMCB{
int type;
int size;
int memorySize;
}lmcb;



typedef struct Block{
int size ;
cmcb*  head;
cmcb*  tail;

}Block; 


///Initializes the heap
void init_heap();
///Creates a memory block
cmcb* makeMemBlock();
///Allocates memeory
u32int allocate(u32int size);
///Shows list of memory
void show_list();
///Shows the free memory
void show_free();
///Shows the allocated memory
void show_allocated();
///Frees the memory
int Free_mem();
///Boolean for if the memory is heap is empty
int IsEmpty();


#endif
