/*!
 * \file R6.h
 */

#ifndef R6_H
#define R6_H
#include <system.h>
#include "../modules/mpx_supt.h"

// Serial Port Setup Addresses for COM1
#define BASE                0x3F8     // The base address of the seven-port sequence for COM1
#define BUFFERS            BASE + 0  // Transmitter Holding Buffer (W) and Receiver Buffer (R) when DLAB=0
#define BAUD_RATE_LSB      BASE +0   // Baud rate divisor LSB Least Sig. Byte (R/W) when DLAB=1
#define INTERRUPT_ENABLE   BASE + 1  // Interrupt Enable
#define BAUD_RATE_MSB      BASE + 1  // Baud rate divisor MBS Byte (R/W) when DLAB=1
#define INTERRUPT_ID       BASE + 2  // Interrupt Identification (R)
#define LINE_CONTROL       BASE + 3  // Line Control Register (R/W)
#define MODEM_CONTROL      BASE + 4  // Modem Control Register (R/W)
#define LINE_STATUS        BASE + 5  // Line Status Register (R) (bit 7 is DLAB bit)
#define MODEM_STATUS       BASE + 6  // Modem Status Register (R)
#define INTVECT            0x0C      // COM1's IRQ Base Address

#define RING_SIZE 	100
#define PIC_MASK           0x21 // interrupt contral mask
#define PIC_REG            0x20  // nterrupt contral command
#define PIC_EOI            0x20 // end of interrupt code
#define IRQ_COM1           0x10

// com_open Return Values
#define OPEN_SUCCESS     0      // return no error
#define OPEN_NULL        -101   //invalid (null) event flag pointer
#define OPEN_INV_BAUD    -102   // invalid baud rate divisor
#define ALREADY_OPEN      -103   //port already open


// com_close Return Values
#define CLOSE_SUCCESS    0
#define CLOSE_NOT_OPEN   -201

// com_write Return Values
#define WRITE_SUCCESS    0
#define WRITE_NOT_OPEN   -401
#define WRITE_INV_BUFF   -402
#define WRITE_INV_COUNT  -403
#define WRITE_DEV_BUSY   -404

// com_read Return Values
#define READ_SUCCESS    0
#define READ_NOT_OPEN   -301
#define READ_INV_BUFF   -302
#define READ_INV_COUNT  -303
#define READ_DEV_BUSY   -304
#define WRITING   5555
#define READING   4444
#define WAITING   6666

#define OPEN     0
#define CLOSED   1

#define Status_OPEN 3


/*for INTERRUPT_ENABLE and BAUD_RATE_LSB and BAUD_RATE_MSB
mean that if bate 7 of line control is 1, then the base and base+1
are baud rate divisor otherwise base+1 is the interrupt enable*/

///Device Control Block struct
typedef struct DCB {
	  int  flag_status; //OPENED or CLOSED
	  int  *Event_Flag; // event flag pointer
    int  Status; // IDLE, READING, WRITING,OPEN
    //*for input
    char *In_Buffer; // pointer to requestors buffer (where read data is will be placed)    *for input
    int  *In_Count;  // how many characters to read
    int  In_Position; //what the position is inside the buffer (ie a left arrow changes the position)
    //*for output
    char *Out_Buffer; // pointer to requestors buffer (where read data is will be placed)   *for output
    int  *Out_Count;  // how many characters to write
    int  Out_Position;  //what the position is inside the buffer (ie a left arrow changes the position)
    // ring buffer array
    char Ring_Buffer[300];
    int  input_index; // index where you will write the next character to the ring buffer
    int  output_index;  // index where you will remove the next character from the  buffer
    int  count;       //count of how many character stored but not read from the buffer
} dcb;

/*!
+*  struct iod represents an I/O Desriptor.
+*  @param pcb_id the process that this iod is representing.
+*  @param op_code the operation that the process requested.
+*  @param com_port the COM port. (You can omit this and just always use COM1)
+*  @param buffer_ptr the buffer pointer to read to/write from.
+*  @param count_ptr the amount of characters to be read/written.
+*  @param next the next IOD in the IO queue after this one.
*/


/*typedef struct IOD{
  char name[12];
  int Op_Code;
  struct PCB* pcb_id ; //the ID of the PCB that requested IO, its character buffer
  char * iod_buffer;
  int * iod_count;
  struct IOD* next_iod;

}IOD;

struct IOCB {
  int event_flag;
	int iocb_count;
  struct IOD * active;
	struct IOD * head;
	struct IOD * tail;
};*/

/*!
+*  pic_mask() masks so only the desired PIC interrupt is enabled or disabled.
+*  @param enable 1 to enable or 0 to disable.
*/
void pic_mask(char enable);

/*!
+*  disable_interrupts() disables all interrupts to device.
*/
void disable_interrupts();

/*!
+*  enable_interrupts() enables interrupts to device.
*/
void enable_interrupts();

/*!
+*  com_open() Opens the communication port.
+*  @param e_flag event flag will be set to 1 if read/write
+*  @param baud_rate the desired baud rate
+*  @return Returns three possible error codes, or a 0 for successful operation.
*/
int com_open(int* e_flag, int baud_rate);


/*!
+*  com_close() Closes the communication port.
+*  @return error code if port was not open, or a 0 for successful operation
*/
int com_close(void);

/*!
+*  com_read() Reads the buffer from the port. Non-blocking.
+*  @param buf_ptr buffer in which the read characters will be stored.
+*  @param count_ptr the maximum number of bytes to read. After completion,
+*                       this will contain the number of characters read.
+*  @return Returns four possible error codes, or a 0 for successful operation.
*/
int com_read(char* buf_ptr, int* count_ptr);

/*!
+*  com_write() Writes the buffer to the port. Non-blocking.
+*  @param buf_ptr buffer in which the characters to write are stored.
+*  @param count_ptr the number of characters from the buffer to write.
+*  @return Returns four possible error codes, or a 0 for successful operation.
*/
int com_write(char* buf_ptr, int* count_ptr);

/*!
+*  serial_io() is the interrupt C routine for serial IO.
*/
void serial_io();

/*!
+*  serial_write() provides interrupt routine for writing IO.
*/
void serial_write();

/*!
+*  serial_read() provides interrupt routine for reading IO.
*/

void serial_read();
#endif
