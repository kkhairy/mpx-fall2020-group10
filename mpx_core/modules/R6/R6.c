#include <core/serial.h>
#include "../modules/mpx_supt.h"
#include <string.h>
#include "../modules/R2/pcb.h"
#include "../modules/R3/syscall.h"
#include "../modules/R2/user_commands.h"
#include <core/io.h>
#include<stdint.h>
#include <system.h>
#include "../modules/R1/date.h"
#include <mem/heap.h>
#include "../modules/R2/pcb.h"
#include "R6.h"

long old;
dcb *port;
static char io;

int mask;

void disable_interrupts() {
  outb(INTERRUPT_ENABLE, 0x00); //disable interrupts
}

void enable_interrupts() {
    outb(COM1 + 4, 0x0B); //enable interrupts, rts/dsr set

}

//void pic_mask(char enable) {
  // If enable, do a logical NOT on the IRQ for COM1.
  // Obtain the mask by inb(the PIC_MASK register).
  // outb (PIC MASK register, (logical AND the mask with the irq from step 1))
//}

int com_open(int *eflag_p, int baud_rate) {
  int Baud_Rate_Div;
  int Return_Val = OPEN_SUCCESS;

  if(port->flag_status){
		Return_Val = ALREADY_OPEN;
	}
  else if (eflag_p == NULL || eflag_p == 0){
		Return_Val = OPEN_NULL;
	}

  else if (baud_rate < 0){
		Return_Val = OPEN_INV_BAUD;
	}
  else {
    // Initialize port DCB
    port->flag_status = OPEN;
    port->Event_Flag = eflag_p;
    port->Status = IDLE;
    port->input_index = 0;
    port->output_index = 0;
    port->count = 0;

    // Set up New Interrupts
    outb(LINE_CONTROL,  0x00);             // Set DLAB = 0
    disable_interrupts();             // Turn off interrupts

    old = (inb(INTVECT));     // Save Old Interrupt
    outb(INTVECT, &serial_io);          // Set New Interrupt

    Baud_Rate_Div = 115200 / (long) baud_rate;

    outb(LINE_CONTROL,  0x80);             // Set DLAB = 1
    outb(BAUD_RATE_LSB, Baud_Rate_Div & 0xFF);    // Set Baud rate - Divisor Latch Low Byte
    outb(BAUD_RATE_MSB, (Baud_Rate_Div >> 8) & 0xFF); // Set Baud rate - Divisor Latch High Byte
    outb(LINE_CONTROL,  0x03);             // 8 Bits, No Parity, 1 Stop Bit
    disable_interrupts();
    outb(PIC_MASK, (inb(PIC_MASK) & ~0x80));// Set Programmable Interrupt Controller
    enable_interrupts();
    outb(MODEM_CONTROL, 0x08);             // Enable Serial Interrupts
    outb(INTERRUPT_ENABLE,     0x01);             // Enable Input Ready Interrupts
  }

  return Return_Val;
}

int com_close() {
  int Return_Val = CLOSE_SUCCESS;

  if (!port->flag_status) {
		Return_Val = CLOSE_NOT_OPEN;
	}
  else {
    port->flag_status = FALSE;
    // Reset Communication Protocol to original settings
    outb(LINE_CONTROL, 0x00);         // Set DLAB = 0
    outb(MODEM_CONTROL, 0x00);        // Disable Serial Interrupts
           // Turn off interrupts
    disable_interrupts();
    outb(PIC_MASK, (inb(PIC_MASK) & 0x10)); // Set Programmable Interrupt Controller
    enable_interrupts();
    outb(INTVECT, old); // Restore Old Interrupt Vector
  }
  return Return_Val;
}



int com_write(char *buf_p, int *count_p) {
  int Return_Value = WRITE_SUCCESS;

  if      (port->flag_status != OPEN){
		Return_Value = WRITE_NOT_OPEN;
	}
  else if (port->Status != IDLE) {
		Return_Value = WRITE_DEV_BUSY;
	}
  else if (buf_p == 0)          {
		Return_Value = WRITE_INV_BUFF;
	}
  else if (count_p == 0)       {
		Return_Value = WRITE_INV_COUNT;
	}
  else {
    port->Out_Buffer = buf_p;
    port->Out_Count = count_p;
    port->Out_Position = 0;
    port->Status = WRITING;
    *port->Event_Flag = 0;

    // Write first char to com port
    outb(BASE, *port->Out_Buffer);
    port->Out_Buffer++;
    port->Out_Position++;

	  outb(INTERRUPT_ENABLE, (inb(INTERRUPT_ENABLE) | 0x02));
  }
  return Return_Value;
}


int com_read(char *buf_p, int *count_p) {
  int Return_Value = READ_SUCCESS;

  if(port->flag_status != OPEN){
		Return_Value = READ_NOT_OPEN;
	}
  else if (port->Status != IDLE) {
		Return_Value = READ_DEV_BUSY;
	}
  else if (buf_p == NULL)          {
		Return_Value = READ_INV_BUFF;
	}
  else if (count_p == NULL)       {
		Return_Value = READ_INV_COUNT;
	}

  port->In_Buffer = buf_p;
  port->In_Count = count_p;
  port->In_Position = 0;
  *port->Event_Flag = 0;
  disable_interrupts();
  port->Status = READING;

  while(port->count > 0){
    if(*port->In_Count == port->In_Position){
      port->Status = IDLE;
      break;
    }

    if(port->Ring_Buffer[port->output_index] == '\r'){
      port->Status = IDLE;
      port->output_index++;
      if(port->output_index > (RING_SIZE - 1)){
        port->output_index = 0;
      }
      break;
    }
    port->In_Buffer[port->In_Position] = port->Ring_Buffer[port->output_index];
    port->In_Position++;
    port->count--;
    port->output_index++;

    if(port->output_index > (RING_SIZE - 1)){
      port->output_index = 0;
    }
  }
  enable_interrupts();

  if(port->Status == IDLE){
    port->In_Buffer[port->In_Position]  = '\0';
    *port->In_Count = port->In_Position;
    *port->Event_Flag = 1;
  }
  return Return_Value;
}
// check port open.
// obtain interrupt type. Call appropriate second level handler
// Check if the event has completed. If so call io scheduler.
// outb(PIC register, PIC end of interrupt)

void  serial_io() {
	static int type;
	if(port->flag_status == OPEN){
		type = inb(INTVECT);
		type = type & 0x07;
		if(type == READ)
				serial_read();//1200 BAUD TO BE USED
		else if (type == WRITE)
			serial_write();
	}
	outb(PIC_REG, PIC_EOI);
}

// Ensure the dcb status is writing
// If there are any more characters left in the buffer, print them
// Othewise we are done printing
// Update the dcb status. Disable output interrupts
void serial_write() {
	static char mask;
	if(port->Status == WRITING){
		if(port->Out_Position != *(port->Out_Count)){
			io = port->Out_Buffer[port->Out_Position];
			port->Out_Position++;
			outb(BASE, io);
		}
		else{
			port->Status = IDLE;
			*port->Event_Flag = 1;
			mask = inb(INTERRUPT_ENABLE);
			mask = mask & ~0x02;
			outb(INTERRUPT_ENABLE, mask);

		}
	}
}

// Ensure the dcb status is reading. If not, push to the ring buffer.
// Read a character from the COM port & add it to the buffer.
// If we reached a new line or the buffer size, we are done reading
// Update the dcb status. Disable intput interrupts

void serial_read() {
	unsigned char input = inb(BASE);
	if(port->Status == READING){
		if(input != '\r'){
			port->In_Buffer[port->In_Position] = input;
			port->In_Position++;
		}
		if(input == '\r' || port->In_Position == *(port->In_Count)){
				port->In_Buffer[port->In_Position] = '\0';
				port->Status = IDLE;
				*port->Event_Flag = 1;
				*port->In_Count = port->In_Position;
		}
	}
	else{
		if(port->count < RING_SIZE){
			port->Ring_Buffer[port->input_index] = input;
			port->input_index++;
			port->count++;
			if(port->input_index > (RING_SIZE - 1)){
				port->input_index = 0;
			}
		}
	}
}
