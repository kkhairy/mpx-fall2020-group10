#include "../modules/mpx_supt.h"

#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include <stdint.h>

char cmdBuffer[100];
int bufferSize = 99;

int bcdToDec(unsigned char temp){

	unsigned char b =  temp & 0x0F ;
	int c = (temp >> 4);
	int ret = (c * 10) + b;

	return ret;
}

unsigned int decToBcd(int temp){
	return ((temp / 10) << 4 | (temp % 10));
}

void itoa( int num, char* str){
	int i =0;

	while(num != 0){
		int digit = num % 10;
		str[i++] = digit + '0';
		num = num / 10;

	}
	str[i] = '\0';

	int a,b;
	char temp;

	for( b = 0, a = i-1; b < a; b++, a--){
		temp = str[b];
		str[b] = str[a];
		str[a] = temp;
	}

	if(str[0] == '\0'){
		strcpy(str,"0");
	}
}

void getDate(){

  	//to get the value of day;
	outb(0x70,0x07); //
	unsigned char bcdDay = inb(0x71); // to get the day value from register in bcd
	int day = bcdToDec(bcdDay); // converts bcd value to decimal
	char dayPtr [3];
	itoa(day,dayPtr); // converts int to char

	//to get the value of month;
	outb(0x70,0x08);
	unsigned char bcdMonth = inb(0x71);
	int month = bcdToDec(bcdMonth);
	char monthPtr [3];
	itoa(month,monthPtr);

	//to get the value of year;
	outb(0x70,0x09);
	unsigned char bcdYear = inb(0x71);
	int year = bcdToDec(bcdYear);

	//to get the century
	outb(0x70,0x37);
	unsigned char bcdCen = inb(0x71);
	int cen = bcdToDec(bcdCen);

	char yearPtr [3];
	int calc = (cen * 100 )+ year;
	itoa(calc,yearPtr);
	//seconds 0x00, minute 0x02 hour 0x04 day week 0x06  day month 0x07 month0x08  year0x09

	sys_req(WRITE, DEFAULT_DEVICE, "\033[0;33m Date:\t", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,  monthPtr, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,  "-", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,  dayPtr, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,  "-", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,  yearPtr, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE,  "\n", &bufferSize);
}

void getTime(){


  	//to get the value of seconds;
	outb(0x70,0x00);
	unsigned char bcdSec = inb(0x71);
	int sec = bcdToDec(bcdSec);
	char secPtr [3];
	itoa(sec,secPtr);


	//to get the value of minutes;
	outb(0x70,0x02);
	unsigned char bcdMinutes = inb(0x71);
	int minutes = bcdToDec(bcdMinutes);
	char minutesPtr[3];
	itoa(minutes,minutesPtr);

	//to get the value of hours;
	outb(0x70,0x04);
	unsigned char bcdHours = inb(0x71);
	int hours = bcdToDec(bcdHours);
	char hoursPtr[3];
	itoa(hours,hoursPtr);

	sys_req(WRITE, DEFAULT_DEVICE, "Time:\t", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, hoursPtr, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, ":", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, minutesPtr, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, ":", &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, secPtr, &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "\n", &bufferSize);
}

int Time(){


  	//to get the value of seconds;
	outb(0x70,0x00);
	unsigned char bcdSec = inb(0x71);
	int sec = bcdToDec(bcdSec);


	//to get the value of minutes;
	outb(0x70,0x02);
	unsigned char bcdMinutes = inb(0x71);
	int minutes = bcdToDec(bcdMinutes);

	//to get the value of hours;
	outb(0x70,0x04);
	unsigned char bcdHours = inb(0x71);
	int hours = bcdToDec(bcdHours);

	int time = hours*10000 + minutes*100 + sec;
	return time;
}
void setDate(int month, int day, int year){
	//disable interrupt
	cli();

	unsigned int monthB = decToBcd(month);
	//set month
	outb(0x70,0x08);
	outb(0x71,monthB);

	unsigned int dayB = decToBcd(day);
	//set day
	outb(0x70,0x07);
	outb(0x71,dayB);

	int century = year/100;
	year = year % 100;


	unsigned int yearB = decToBcd(year);
	//set year
	outb(0x70,0x09);
	outb(0x71,yearB);

	unsigned int cenB = decToBcd(century);
	//set century
	outb(0x70,0x37);
	outb(0x71,cenB);

	//enable interrupt
	sti();
}

void setTime(int hour, int min, int second){
	//disable interrupt
	cli();

	unsigned int secondB = decToBcd(second);
	//set second
	outb(0x70,0x00);
	outb(0x71,secondB);

	unsigned int minutesB = decToBcd(min);
	//set minute
	outb(0x70,0x02);
	outb(0x71,minutesB);

	unsigned int hourB = decToBcd(hour);
	//set hour
	outb(0x70,0x04);
	outb(0x71,hourB);


	//enable interrupt
	sti();
}
