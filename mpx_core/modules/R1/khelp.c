#include "date.c"
#include "../modules/mpx_supt.h"
//#include <mem/heap.h>

#include <string.h>
#include <core/serial.h>
#include <core/io.h>

#include<stdio.h>
#include<conio.h>


int bufferSize = 100;

sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m    __ \033[1;34m   ___________  \033[1;32m  __             __   _    _________      \n \n " , &bufferSize);
sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m   / /  \033[1;34m / /        \ \ \033[1;32m \ \          / /   | |   | |    \ \      \n \n " , &bufferSize);
sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m  / /  \033[1;34m / /          \ \  \033[1;32m\ \        / /    | |   | |     \ \    \n \n " , &bufferSize);
sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m  \ \  \033[1;34m \ \          / /   \033[1;32m\ \      / /     | |   | |     / /    \n \n " , &bufferSize);
sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m   \ \  \033[1;34m \ \        / /   \033[1;32m  \ \    / /      | |   | |    / /     \n \n " , &bufferSize);
sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m    \_\ \033[1;34m  \_\______/_/      \033[1;32m \_\__/_/       |_|   |_|___/_/      \n \n " , &bufferSize);