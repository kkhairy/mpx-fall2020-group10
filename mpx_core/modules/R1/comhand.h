/*!
 * \file comhand.h
 */


#ifndef COMHAND_H
#define COMHAND_H

#include "date.h"
#include "../modules/mpx_supt.h"
#include <system.h>

#include "../modules/R2/pcb.h"
#include "../modules/R3/syscall.h"
#include "../modules/R3/procsr3.h"
#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include "../modules/R5/r5.h"


#include<stdint.h>

#define EXIT 0
#define IDLE 1
#define READ 2
#define WRITE 3
#define INVALID_OPERATION 4

#define TRUE  1
#define FALSE  0

#define MODULE_R1 0
#define MODULE_R2 1
#define MODULE_R3 2
#define MODULE_R4 4
#define MODULE_R5 8
#define MODULE_F  9
#define IO_MODULE 10
#define MEM_MODULE 11

// error codes
#define INVALID_BUFFER 1000
#define INVALID_COUNT 2000
#define INVALID_COMMAND 2001

#define DEFAULT_DEVICE 111
#define COM_PORT 222

///Processes user input and delivers appropriate output.
void comhand();
///Prints the startup menu.
void print_startup();
///Prints the current OS version.
void version();
///Exits the OS after a confirmation
void exit();
///Prints a description of each command.
void help();
///Clears the current screen
void clear_screen();
///Prints the corresponding error message
void print_error( int err);
#endif