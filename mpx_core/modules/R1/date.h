/*!
 *\file date.h
 */

#ifndef DATE_H
#define DATE_H

///Display's the date (MM/DD/YYYY)
void getDate();

///Display's the time (HH:MM:SS)
void getTime();

int Time();
///Prompt's user for the date (MM/DD/YYYY)
void setDate();

///Prompt's user for the time (HH:MM:SS)
void setTime();

///Converts an integer to a string
void itoa( int num, char* str);

///Converts decimal value to binary coded decimal value
unsigned int decToBcd(int temp);


#endif
