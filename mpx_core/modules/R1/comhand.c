//#include "date.c"
#include "date.h"
#include "comhand.h"
#include "../modules/mpx_supt.h"
#include "../modules/R2/user_commands.h"
#include "../modules/R2/pcb.h"
#include "../modules/R5/r5.h"

#include "../modules/R3/syscall.h"
#include "../modules/R3/procsr3.h"
#include "../modules/R3/R4.h"

#include <string.h>
#include <core/serial.h>
#include <core/io.h>
#include "../modules/R6/newTestProcs.c"


#include<stdint.h>

int quit;

//R1 Basic Interface Development
/*R2 - PCB Operations
The functions necessary for managing processes will be implemented.
R3/R4 - Dispatching
Processes will be loaded and will be given CPU time.
Command handler will become a process.
R5 - Memory Management
A heap manager (i.e. free() and malloc()).
R6 - ?????? (TBD) */

void comhand(){
	char cmdBuffer[100];
	int bufferSize = 100;
	//int response = 1;
    sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;31m*******\t\033[1;34m *********  \033[1;32m **      **   ***   *****          \n \n" , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;31m**\t\t\033[1;34m **     **  \033[1;32m  **    **    ***  	**  ** \n \n" , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;31m**\t\t\033[1;34m **     **  \033[1;32m   **  **     ***  	**    **  \n \n" , &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;31m**\t\t\033[1;34m **     **  \033[1;32m   **  **     ***  	**    **  \n \n" , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;31m**\t\t\033[1;34m **     **  \033[1;32m    ****      ***  	**  **       \n \n" , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;31m*******\t\033[1;34m *********  \033[1;32m     **       ***   *****   \n \n" , &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE, "\n \n \033[0;34m*****************************************************\n", &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE, "\n **              Welcome to COVID-OS!               ** \n ", &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE,"\n ***************************************************** \n ", &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "\n \033[1m             What would you like to do? \n For \n [H]ELP : Type H or h \n [G]ET [D]ATE : GD or gd \n", &bufferSize);
	print_startup();

	quit =0;
    while(!quit){

		//reinitialize buffer and reset buffer size variable
		memset(cmdBuffer, '\0', 100);
		bufferSize = 99;

		//sys_rew READ to read input from user
		//CALLS POLLING
		sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize); //will return result of polling function

		//check for help
		if((cmdBuffer[0] == 'H' || cmdBuffer[0] == 'h' ) && (cmdBuffer[1] == '\r')){
			help();
			//if (!=help())
			//return error(help());

		}
		//check for version
		else if
		((cmdBuffer[0] == 'V' || cmdBuffer[0] == 'v') && (cmdBuffer[1] == '\r'))
		{
			version();
		}
		//check if user chose to get date;(GD)
		else if
		(((cmdBuffer[0] == 'G' && cmdBuffer[1] == 'D')||(cmdBuffer[0] == 'g' && cmdBuffer[1] == 'd')) && (cmdBuffer[2] == '\r'))
		{
			getDate();
		}
		//check if user chose to get time;(GT)
		else if
		(((cmdBuffer[0] == 'G' && cmdBuffer[1] == 'T')||(cmdBuffer[0] == 'g' && cmdBuffer[1] == 't')) && (cmdBuffer[2] == '\r' ))
		{
			getTime();
		}

		//check if user chose to set date;(SD)
		else if((cmdBuffer[0] == 'S' || cmdBuffer[0] == 's') && (cmdBuffer[1] == 'D' || cmdBuffer[1] == 'd') && cmdBuffer[2] == '\r' ){
			int cond = 1; 	// cond for invalid input exiting
			while(cond){

				sys_req(WRITE, DEFAULT_DEVICE, "Enter the date. (MM/DD/YYYY) \n", &bufferSize);
				memset(cmdBuffer, '\0',100);
				sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);

				char* token = strtok(cmdBuffer,"/");
				int month = atoi(token);

				token = strtok(NULL,"/");
				int day = atoi(token);


				token = strtok(NULL,"\r");
				//token[4] = '\0';
				int year= atoi(token);

				itoa(year,token);
				sys_req(WRITE, DEFAULT_DEVICE, token, &bufferSize);

				itoa(year,token);
				//sys_req(WRITE, DEFAULT_DEVICE, token, &bufferSize);

				if((day > 0 && day <= 31 ) && (month  > 0 && month <= 12) && (year > 1000  && year <= 3000)){

					setDate(month, day, year);
					cond =0;

				}
				else if(cond && ((cmdBuffer[0] == 'x' || cmdBuffer[0] == 'X') && (cmdBuffer[1] == '\r' ))){
					cond = 0;
					print_startup();
				}
				else{
					sys_req(WRITE, DEFAULT_DEVICE, "Invalid selection.\nPress X if you want to exit setting date.\n", 						&bufferSize);
				}
			}
		}
		//check if user chose to set time;(ST)
		else if((cmdBuffer[0] == 'S' || cmdBuffer[0] == 's') && (cmdBuffer[1] == 'T' || cmdBuffer[1] == 't') && (cmdBuffer[2] == '\r')){
			int cond = 1; 	// cond for invalid input exiting
			while(cond){

				sys_req(WRITE, DEFAULT_DEVICE, "Enter the time. (HH:MM:SS) \n", &bufferSize);
				memset(cmdBuffer, '\0',100);
				sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);

				char* token = strtok(cmdBuffer,":");
				int hours = atoi(token);

				token = strtok(NULL,":");
				int minutes = atoi(token);

				token = strtok(NULL,"\r");
				//token[2] = '\0';
				int seconds = atoi(token);

				if((hours > 0 && hours <= 23 ) && (minutes  > 0 && minutes <= 59) &&( seconds > 0 && seconds <= 59)){
					setTime(hours, minutes, seconds);
					cond =0;
				}
				else if(cond && ((cmdBuffer[0] == 'x' || cmdBuffer[0] == 'X') && (cmdBuffer[1] == '\r' ))){
					cond = 0;
					print_startup();
				}
				else{
					sys_req(WRITE, DEFAULT_DEVICE, "Invalid selection.\nPress X if you want to exit setting time.\n", 						&bufferSize);
				}
			}

		}
      		//check if user chose to exit;
		else if(((cmdBuffer[0] == 'E') || (cmdBuffer[0] == 'e')) && (cmdBuffer[1] == '\r'))
		{   exit();

		}
		else if((cmdBuffer[0]=='D' || cmdBuffer[0]=='d' ) && (cmdBuffer[1]=='e'|| cmdBuffer[1]=='E' ) && (cmdBuffer[2]=='l'|| cmdBuffer[2]=='L' ) && cmdBuffer[3]=='\r'){ //delete PCB
			sys_req(WRITE, DEFAULT_DEVICE, "Enter process name for delete !\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *proc_name = strtok(cmdBuffer,"\r");
		    deletePCB(proc_name);
		}
		else if((cmdBuffer[0]=='P' || cmdBuffer[0]=='p' ) && (cmdBuffer[1]=='r'|| cmdBuffer[1]=='R' ) && (cmdBuffer[2]=='i'|| cmdBuffer[2]=='I' ) && cmdBuffer[3]=='\r'){ // Set Priority
		 	sys_req(WRITE, DEFAULT_DEVICE, "enter process name and priority for block !(NAME,PRIORITY)\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);

			char* token = strtok(cmdBuffer,",");
			char* proc_name = token;

			token = strtok(NULL,"\r");
			//token[1] = '\0';
			int priority = atoi(token);
		    	set_pri(proc_name, priority);
		}

        else if((cmdBuffer[0]=='S' || cmdBuffer[0]=='s' ) && (cmdBuffer[1]=='u'|| cmdBuffer[1]=='U' ) && (cmdBuffer[2]=='s'|| cmdBuffer[2]=='S' ) && cmdBuffer[3]=='\r'){ //Suspend PCB
			sys_req(WRITE, DEFAULT_DEVICE, "enter process name for Suspend !\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *proc_name = strtok(cmdBuffer,"\r");
			//strcpy(proc_name, cmdBuffer);
			//*(proc_name+(strlen(cmdBuffer)-1)) = '\0';
		    suspendPCB(proc_name);
		}
		else if((cmdBuffer[0]=='R' || cmdBuffer[0]=='r' ) && (cmdBuffer[1]=='e'|| cmdBuffer[1]=='E' ) && (cmdBuffer[2]=='s'|| cmdBuffer[2]=='S' ) && cmdBuffer[3]=='\r'){ //Resume PCB
			sys_req(WRITE, DEFAULT_DEVICE, "enter process name for Resume !\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);

			char *proc_name = strtok(cmdBuffer,"\r");
			//strcpy(proc_name, cmdBuffer);
			//*(proc_name+(strlen(cmdBuffer)-1)) = '\0';
		   	 resumePCB(proc_name);
		}
		else if((cmdBuffer[0]=='S' || cmdBuffer[0]=='s') && (cmdBuffer[1]=='A' || cmdBuffer[1]=='a') && (cmdBuffer[2] == '\r')){
			show_all();

		}
		else if((cmdBuffer[0]=='S' || cmdBuffer[0]=='s' ) && (cmdBuffer[1]=='p'|| cmdBuffer[1]=='P' ) && (cmdBuffer[2]=='c'|| cmdBuffer[2]=='C' ) && (cmdBuffer[3]=='b'|| cmdBuffer[3]=='B') && cmdBuffer[4]=='\r'){ //Show PCB
			sys_req(WRITE, DEFAULT_DEVICE, "enter process name for show !\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *proc_name = strtok(cmdBuffer,"\r");
			//strcpy(proc_name, cmdBuffer);
			//*(proc_name+(strlen(cmdBuffer)-1)) = '\0';
			show_pcb(proc_name);

		}
		else if((cmdBuffer[0]=='S' || cmdBuffer[0]=='s' ) && (cmdBuffer[1]=='r'|| cmdBuffer[1]=='R' ) && cmdBuffer[2]=='\r'){ //show_ready
			show_ready();
		}

		else if((cmdBuffer[0]=='S' || cmdBuffer[0]=='s' ) && (cmdBuffer[1]=='b'|| cmdBuffer[1]=='B' )  && cmdBuffer[2]=='\r'){ //show_blocked
			show_blocked();
		}
		else if((cmdBuffer[0]=='S' || cmdBuffer[0]=='s' ) && (cmdBuffer[1]=='s'|| cmdBuffer[1]=='S' )  && (cmdBuffer[2]=='r'|| cmdBuffer[2]=='R' ) && cmdBuffer[3]=='\r'){ //show_blocked
			show_susRed();
		}
		//yield
		//else if((cmdBuffer[0]=='Y' || cmdBuffer[0]=='y' ) && (cmdBuffer[1]=='l'|| cmdBuffer[1]=='L' )  && (cmdBuffer[2]=='D'|| cmdBuffer[2]=='d' ) && cmdBuffer[3]=='\r'){
	//		asm volatile("int $60");
		//}
		else if((cmdBuffer[0]=='A' || cmdBuffer[0]=='a' ) && (cmdBuffer[1]=='l'|| cmdBuffer[1]=='L' )  && (cmdBuffer[2]=='A'|| cmdBuffer[2]=='a' ) && cmdBuffer[3]=='\r'){
			sys_req(WRITE, DEFAULT_DEVICE, "enter the message for the alarm and time. (msg)/(HH:MM:SS)!\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *procMsg = strtok(cmdBuffer,"/");

			char* token = strtok(NULL, ":");
			int hour = atoi(token);

			token = strtok(NULL, ":");
			int min = atoi(token);

			token = strtok(NULL, "\r");
			int sec = atoi(token);

			createAlarm(procMsg, hour, min, sec);

		}
		else if((cmdBuffer[0]=='l' || cmdBuffer[0]=='L' ) && (cmdBuffer[1]=='o'|| cmdBuffer[1]=='O' )  && (cmdBuffer[2]=='a' || cmdBuffer[2]=='A' ) && (cmdBuffer[3]=='D'|| cmdBuffer[3]=='d' ) && cmdBuffer[4]=='\r'){
			load(&proc1, "p1", 1);
			load(&proc2, "p2", 2);
			load(&proc3, "p3", 3);
			load(&proc4, "p4", 4);
			load(&proc5, "p5", 5);
			load(&COMREAD, "read", 1);
			load(&COMWRITE, "write", 2);
			load(&IOCOM25, "25", 3);
			load(&IOCOM, "com", 3);


		}
		else if((cmdBuffer[0]=='I' || cmdBuffer[0]=='i' ) && (cmdBuffer[1]=='n'|| cmdBuffer[1]=='N' )  && (cmdBuffer[2]=='F' || cmdBuffer[2]=='f' ) && cmdBuffer[3]=='\r'){
			run_infinite();
		}
		else if((cmdBuffer[0]=='a' || cmdBuffer[0]=='A' ) && (cmdBuffer[1]=='l'|| cmdBuffer[1]=='L' )  && (cmdBuffer[2]=='o' || cmdBuffer[2]=='O' ) && cmdBuffer[3]=='\r'){
			sys_req(WRITE, DEFAULT_DEVICE, "Enter the memory to allocate\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			//serial_println("uuuuuuuuuuuu");
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *mem = strtok(cmdBuffer,"\r");
			u32int bytes = (u32int)atoi(mem);
			allocate(bytes);
		}
		else if((cmdBuffer[0]=='I' || cmdBuffer[0]=='i' ) && (cmdBuffer[1]=='N' || cmdBuffer[1]=='n' )  && (cmdBuffer[2]=='i' || cmdBuffer[2]=='I' ) && cmdBuffer[3]=='\r'){
			sys_req(WRITE, DEFAULT_DEVICE, "Enter the memory for heap", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *mem = strtok(cmdBuffer,"\r");
			int bytes = atoi(mem);
			init_heap(bytes);
		}

		else if((cmdBuffer[0]=='f' || cmdBuffer[0]=='F' ) && (cmdBuffer[1]=='r'|| cmdBuffer[1]=='R' )  && (cmdBuffer[2]=='e' || cmdBuffer[2]=='E' ) && (cmdBuffer[3]=='M' || cmdBuffer[3]=='m' )
		&& (cmdBuffer[4]=='e' || cmdBuffer[4]=='e' )&& (cmdBuffer[5]=='M' || cmdBuffer[5]=='m' ) && cmdBuffer[6]=='\r')
		{
			sys_req(WRITE, DEFAULT_DEVICE, "Enter the address for freeing memory\n", &bufferSize);
			memset(cmdBuffer, '\0',100);
			sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
			char *mem = strtok(cmdBuffer,"\r");
			int addr = atoi(mem);
            Free_mem((u32int)addr);
		}

		else if((cmdBuffer[0]=='i' || cmdBuffer[0]=='I' ) && (cmdBuffer[1]=='s'|| cmdBuffer[1]=='S' )  && (cmdBuffer[2]=='e' || cmdBuffer[2]=='E' ) && (cmdBuffer[3]=='M' || cmdBuffer[3]=='m' )
		&& (cmdBuffer[4]=='p' || cmdBuffer[4]=='P' )&& (cmdBuffer[5]=='t' || cmdBuffer[5]=='T' ) && cmdBuffer[6]=='\r')
		{
            if(IsEmpty() ){
				sys_req(WRITE, DEFAULT_DEVICE, " The heap is empty\n", &bufferSize);
			}
			else{
				sys_req(WRITE, DEFAULT_DEVICE, " The heap is not empty\n", &bufferSize);
			}
		}

		else if((cmdBuffer[0]=='s' || cmdBuffer[0]=='S' ) && (cmdBuffer[1]=='f'|| cmdBuffer[1]=='F' )  && (cmdBuffer[2]=='m' || cmdBuffer[2]=='M' ) && cmdBuffer[3]=='\r'){
			show_free();
		}
		else if((cmdBuffer[0]=='s' || cmdBuffer[0]=='S' ) && (cmdBuffer[1]=='a'|| cmdBuffer[1]=='A' )  && (cmdBuffer[2]=='m' || cmdBuffer[2]=='M' ) && cmdBuffer[3]=='\r'){
			show_allocated();
		}
		else if((cmdBuffer[0]=='C' || cmdBuffer[0]=='c' ) && (cmdBuffer[1]=='l'|| cmdBuffer[1]=='L' )  && (cmdBuffer[2]=='r' || cmdBuffer[2]=='R' ) && cmdBuffer[3]=='\r'){
			clear_screen();
		}
		else{
			sys_req(WRITE, DEFAULT_DEVICE, "No Such Commands\n", &bufferSize);
		}
		sys_req(IDLE, DEFAULT_DEVICE, NULL, NULL);
  }
}


void print_startup(){
	int bufferSize = 100;
	sys_req(WRITE, DEFAULT_DEVICE, "  \033[0m " , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, " \n \033[0;32mCommand list: \n [H]elp \n [V]ersion \n [S]et[D]ate \n [G]et[D]ate \n [G]et[T]ime \n [S]et[T]ime\n" , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, " [C]lear_screen \n [E]xit \n \n" , &bufferSize);

    sys_req(WRITE, DEFAULT_DEVICE, " \n  \033[1;32m *****   PCB User Commands!   *****  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, " \033[0;37m \n [S][u][s]pend \n [R][e][s]ume \n Set [P][r][i]ority\n [S]how [P][C][B]\n [S]how [A]ll\n [S]how [R]eady\n [S]how [B]lock\n " , &bufferSize);
    //sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;32m *****   PCB Temporary Commands!   *****  \n " , &bufferSize);
	//sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;37m\n Create [P]CB \n [D][e][l]ete PCB \n [B][l][o]ck PCB \n [U][n][b]lock PCB \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "\033[0;37m[D][e][l]ete PCB \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, " \n  \033[1;32m *****   R3 & R4  Commands!   *****  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;37m \n [l][o][a][d]  \n [i][n][f]inite\n [a][l][a]rm\n" , &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE, " \n  \033[1;32m *****   R5 Commands!   *****  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  \033[0;37m \n [F][r][e]e [M][e][m]ory  \n [A][l]l[o]cate Memory \n [I][s][E]mpty\n [S]how [F]ree [M]emory \n [S]how [A]llocated [M]emory \n", &bufferSize);
	//return 0;
}

void help(){
	int bufferSize =100;
	sys_req(WRITE, DEFAULT_DEVICE, "  \033[1;31m *****  Welcome to COVID-OS help menu! ****** \n \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, " \033[0;32m Help: \n To access COVID-OS system information. \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Version: \n Print the Current System Verison.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  SetDate: \n To set or update the date    .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  GetDaten: \n To get and print current date in Month_Date_Year   .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  GetTime: \n To get and print current time in Hour:Mintue:Second . \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  SetTime: \n To set or update the time .  \n " , &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE, "  Suspend: \n Suspend the current pcb  .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Resume: \n Resume the current pcb.  \n " , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "  Set Priority:\n  Sets the priority of a pcb,int between [0 and 9] lower execute after ligher value.  \n " , &bufferSize);
    sys_req(WRITE, DEFAULT_DEVICE, "  Show PCB: \n Shows all the information contained by the pcb [Name,class,status,state, priority] .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Show All: \n Shows all the pcb present in the ready queue and blocked queue .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Show Ready: \n Shows all the pcb present in the ready queue .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Show Block: \n Shows all the pcb present in the blocked queue .  \n " , &bufferSize);


	sys_req(WRITE, DEFAULT_DEVICE, "  Delete PCB: \n  Removes pcb from appropritate queue and frees associated memory. \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  \n PCB Temporary Commands! :  .  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Create PCB: \n Create the pcb.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Block PCB: \n Block the pcb.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Unblock PCB:\n Unblock the pcb.  \n " , &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE, "  \n R3 & R4 Commands.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  load: \n The load command it allow processes3 to load.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  inifinite: \n The inifinet command loads infinite process in suspended ready state.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  alarm: \n The alarm command it creates an alarm process that executes after a certain time.  \n " , &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE, "  \n R5 Commands.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Initialize Heap : \n This function will be used to allocate all the memory available for your MPX.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Allocate Memory : \n This function will be used to allocatememory from the heap. \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Free Memory : \n This procedure will be used to free aparticular block of memory that was previously allocated.  \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  IsEmpty  : \n This function is return True if the heap is empty or False if not ,contains free momory only   \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Show Free Memory  : \n This function will is traverse the list that show the block address and the block size for the free momery   \n " , &bufferSize);
	sys_req(WRITE, DEFAULT_DEVICE, "  Show Allocated Memory  : \n This function will is traverse the list that show the block address and the block size for the allocated momery   \n " , &bufferSize);

	sys_req(WRITE, DEFAULT_DEVICE, "  Exit:\n  Exit COVID-OS system.  \n " , &bufferSize);

	//return 0;
}
void version(){
	int bufferSize = 3;

	sys_req(WRITE, DEFAULT_DEVICE, "\n   \033[0;31m *****  COVID_OS Version 06  Created 12/01/2020  ***** \n " , &bufferSize);

}

void clear_screen(){
	int bufferSize = 100;
	sys_req(WRITE, DEFAULT_DEVICE, "\033[H \033[2J \n " , &bufferSize);

}

void print_error( int err){
	if (err == INVALID_COMMAND){
		sys_req(WRITE, DEFAULT_DEVICE, "\n \033[0;31m Invalid Command!!:  \n" , &bufferSize);
	}
}
void exit(){
	char cmdBuffer[100];
	int bufferSize = 100;

	while(1){

   		sys_req(WRITE, DEFAULT_DEVICE, "\033[0;31m Are you sure you want to Exit ? \n [Y]es \n [N]o \n", &bufferSize);
    	memset(cmdBuffer, '\0',100);
    	sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);

     	if((cmdBuffer[0] == 'Y' || cmdBuffer[0] == 'y') && cmdBuffer[1] == '\r') {
				sys_req(WRITE, DEFAULT_DEVICE, "\033[0;31m ***************************************\n", &bufferSize);
				sys_req(WRITE, DEFAULT_DEVICE, " ****     System is Shut Down !     ****\n", &bufferSize);
				sys_req(WRITE, DEFAULT_DEVICE, "****************************************\n", &bufferSize);
					 //idle();ssss
				 sys_req(EXIT, DEFAULT_DEVICE, NULL, NULL);
				 quit = 0;
	        	//return;
			}
      else if ((cmdBuffer[0] == 'N' || cmdBuffer[0] == 'n') && cmdBuffer[1] == '\r'){
          	    sys_req(WRITE, DEFAULT_DEVICE, "\n \033[0;32m Select Your Choice From The List :  \n", &bufferSize);
								return comhand();
	    }
      else print_error(INVALID_COMMAND) ;

    }
}
