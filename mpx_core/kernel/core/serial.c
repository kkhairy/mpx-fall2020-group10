/*!
 * \file serial.c
 */



/*
  ----- serial.c -----

  Description..: Contains methods and variables used for
    serial input and output.
*/

#include <stdint.h>
#include <string.h>

#include <core/io.h>
#include <core/serial.h>

#define NO_ERROR 0

// Active devices used for serial I/O
int serial_port_out = 0;
int serial_port_in = 0;

/*
  Procedure..: init_serial
  Description..: Initializes devices for user interaction, logging, ...
*/
int init_serial(int device)
{
  outb(device + 1, 0x00); //disable interrupts
  outb(device + 3, 0x80); //set line control register
  outb(device + 0, 115200/9600); //set bsd least sig bit
  outb(device + 1, 0x00); //brd most significant bit
  outb(device + 3, 0x03); //lock divisor; 8bits, no parity, one stop
  outb(device + 2, 0xC7); //enable fifo, clear, 14byte threshold
  outb(device + 4, 0x0B); //enable interrupts, rts/dsr set
  (void)inb(device);      //read bit to reset port
  return NO_ERROR;
}

/*
  Procedure..: serial_println
  Description..: Writes a message to the active serial output device.
    Appends a newline character.
*/
int serial_println(const char *msg)
{
  int i;
  for(i=0; *(i+msg)!='\0'; i++){
    outb(serial_port_out,*(i+msg));
  }
  outb(serial_port_out,'\r');
  outb(serial_port_out,'\n');
  return NO_ERROR;
}

/*
  Procedure..: serial_print
  Description..: Writes a message to the active serial output device.
*/
int serial_print(const char *msg)
{
  int i;
  for(i=0; *(i+msg)!='\0'; i++){
    outb(serial_port_out,*(i+msg));
  }
  if (*msg == '\r') outb(serial_port_out,'\n');
  return NO_ERROR;
}

/*int (*student_read)(char *buffer, int *count);
  Procedure..: set_serial_out
  Description..: Sets serial_port_out to the given device address.
    All serial output, such as that from serial_println, will be
    directed to this device.
*/
int set_serial_out(int device)
{
  serial_port_out = device;
  return NO_ERROR;
}

/*
  Procedure..: set_serial_in
  Description..: Sets serial_port_in to the given device address.
    All serial input, such as console input via a virtual machine,
    QEMU/Bochs/etc, will be directed to this device.
*/
int set_serial_in(int device)
{
  serial_port_in = device;
  return NO_ERROR;
}

int *polling(char *buffer, int *count){
// insert your code to gather keyboard input via the technique of polling.
// You must validat each key and handle special keys such as delete, back space, and
// arrow key
  int i = 0;


  while(1){//sysreq calls polling. will continuously check for characters until enter is entered. i.e POLLING

    char pbuffer[2];
    pbuffer[1] = *"\0";

    if (i == *count){
      return 0;
    }

    if (inb(COM1+5) & 1){ //checks for character input

     	 buffer[i]= inb(COM1); //get character
     	 pbuffer[0] = buffer[i];
    
      if (buffer[i] >= 32 && buffer[i] <= 126){ //check if alphabetic
		      	 serial_print(pbuffer);

        i++;
      }

      if(buffer[i] == 127  ){//backspace
        buffer[i] = NULL;
       // buffer[i-1] = NULL;
        serial_print("\b \b");
        i--;
      }
      if (buffer[i]==126){//delete TO FIX 
      	buffer[i] = NULL;
       
        serial_print("\b \b");
        i--;
      }
	  
      if(buffer[i] == 13){ //enter
        buffer[i+1] = '\0'; //set last char in buffer to null character
        serial_print("\r");
        return NO_ERROR;

      }

    }


  }
return 0;
}
